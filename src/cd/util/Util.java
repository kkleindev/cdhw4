package cd.util;

import cd.ir.Ast;
import cd.ir.Symbol;

import java.util.HashMap;

import static cd.backend.codegen.RegisterManager.*;

/**
 * Created by felix on 02/12/15.
 */
public class Util {

    public static int sizeOf(Ast.ClassDecl classDecl){
        int size = 4;

        // requires 1. pointer to dynamic class vtable, 2. fields
        if (classDecl.sym.isReferenceType()){
            size += 4* classDecl.fields().size();
        }

        return size;

    }

    //copy object for passing by value and return Register in which adress is stored in
    public static Register copyObject(Register regWithAdressOfObjectToBeCopied){
        //TODO (Kevin)
        return Register.EAX;
    }

    public static HashMap<String,Integer> classSizeMap = new HashMap<>();

    public static void initializeSizeMap(Ast.ClassDecl classDecl){
        classSizeMap.put("int",4);
        classSizeMap.put("boolean",4);
        classSizeMap.put("Object",4);
        classSizeMap.put(classDecl.name,sizeOf(classDecl));
    }

}
