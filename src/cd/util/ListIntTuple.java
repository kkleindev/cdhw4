package cd.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kevin on 03.12.15.
 */
public class ListIntTuple {

    // null if field
    public List<String> classes;

    public List<String> methods;

    public int counter;

    public ListIntTuple(){
        classes = new ArrayList<String>();
        methods = new ArrayList<String>();
        counter = 1;
    }

    public void removeMethod (String methodName){
        int index = methods.indexOf(methodName);
        classes.remove(index);
        methods.remove(index);
    }
}
