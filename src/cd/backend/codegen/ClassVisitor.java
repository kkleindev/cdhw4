package cd.backend.codegen;

import cd.Config;
import cd.ir.Ast;
import cd.ir.AstVisitor;
import cd.ir.Symbol;
import cd.util.ListIntTuple;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kevin on 03.12.15.
 */
public class ClassVisitor extends AstVisitor<ListIntTuple,Boolean> {

    protected AstCodeGenerator astCodeGenerator;

    public ClassVisitor (AstCodeGenerator astCodeGenerator){
        super();
        this.astCodeGenerator = astCodeGenerator;
    }

    @Override
    // Methods prints the vtable for every class if boolean is set, else return string
    // Assumes that the data section tag has already been emitted
    public ListIntTuple classDecl(Ast.ClassDecl ast, Boolean emit) {

        // emit classname as Label
        if (emit) {
            astCodeGenerator.emit.emitLabel(Config.CLASS + ast.name);
            // if superclass, emit its label, else emit 0
            String superClassEmission = Config.CLASS + ast.superClass;
            if (ast.superClass == null || ast.sym.superClass.equals(Symbol.ClassSymbol.objectType))
                superClassEmission = "0";
            astCodeGenerator.emit.emitRaw(Config.DOT_INT + " " + superClassEmission);
        }

        // check for methods and ints of superclasses
        ListIntTuple classInformation;
        if (ast.sym.superClass != null && !ast.sym.superClass.equals(Symbol.ClassSymbol.objectType))
            classInformation = classDecl(ast.sym.superClass.ast, false);
        else classInformation = new ListIntTuple();

        // emit own methods
        for (Ast.MethodDecl methodDecl: ast.methods()){
            // test for overriding
            if (classInformation.methods.contains(methodDecl.name)){
                classInformation.removeMethod(methodDecl.name);
                classInformation.counter--;
            }
            methodDecl.sym.offset = classInformation.counter*Config.FIELD_OFFSET;
            classInformation.methods.add(methodDecl.name);
            classInformation.classes.add(ast.name);
            classInformation.counter++;
        }

        // emit field offsets
        classInformation.counter = 1;
        for (Ast.VarDecl varDecl : ast.fields()){
            ast.sym.fieldOffsets.put(varDecl.name, classInformation.counter * Config.FIELD_OFFSET);
            classInformation.classes.add(null);
            classInformation.methods.add(Integer.toString(classInformation.counter * Config.FIELD_OFFSET));
            classInformation.counter++;
        }

        if (emit){
            for (int i = 0; i < classInformation.methods.size(); i++){
                // if field
                if (classInformation.classes.get(i)==null);
                    //astCodeGenerator.emit.emitRaw(Config.DOT_INT + " " + classInformation.methods.get(i));
                else
                    astCodeGenerator.emit.emitRaw(Config.DOT_INT + " " + classInformation.classes.get(i) + "_" + classInformation.methods.get(i));
            }
        }
        return classInformation;
    }

}
