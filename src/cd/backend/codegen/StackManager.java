package cd.backend.codegen;

import cd.Config;
import cd.backend.codegen.RegisterManager.Register;
import cd.ir.Ast;
import cd.util.Tuple;
import cd.util.Util;

import java.util.*;

import static cd.backend.codegen.AssemblyEmitter.constant;
import static cd.backend.codegen.RegisterManager.STACK_REG;
import static cd.ir.Symbol.*;

/**
 * Created by felix on 02/12/15.
 */
public class StackManager extends VirtualRegisterManager {
    /*Stack (grows downwards):
    1. old frame Pointer (offset 0) (done by caller)
    2. local Variables (offset -4...-n)
    3. spilled registers (-n+4 .. -i
    4. parameters (function arguments) (caller)
    5. this reference (caller)
    5. return adress (caller)
     */

    // arguments passed by value,

    //Integer <==> offset; boolean <==> reference type?
    HashMap<String,Tuple<Integer,Boolean>> locals;
    HashMap<String,Tuple<Integer,Boolean>> args;

    // set up the new stack frame
    public StackManager(Ast.MethodDecl ast, AstCodeGenerator cg) {
        super(cg);

        //push old ebp value on stack
        cg.emit.emit("pushl", "%ebp");
        //move ebp to new position
        cg.emit.emitMove(Register.ESP, Register.EBP);
        //lowerOffsetBy(4);

        //add arguments info
        args = new HashMap<>();
        List<VariableSymbol> params = ast.sym.parameters;
        //last push param is right above ebp => reverse
        Collections.reverse(params);
        //first offset =12 because return adress is at 4. this reference at 8.
        int paramsCounter = 12;
        for (VariableSymbol param : params){
            args.put(param.toString(),new Tuple<Integer, Boolean>(paramsCounter,param.type.isReferenceType()));
            paramsCounter += 4;
        }

        //push local variables and add info
        locals = new HashMap<>();
        for (Ast local : ast.decls().children()){
            lowerOffsetBy(4);
            Ast.VarDecl var = (Ast.VarDecl) local;
            cg.emit.emit("pushl", "$0"); //TODO is this correct intitialization?
            locals.put(var.name, new Tuple<Integer, Boolean>(offset, var.sym.type.isReferenceType()));
        }
    }

    public void loadLocalVarAdressInRegister(String name, Register targetReg){

        if (!locals.containsKey(name)) throw new RuntimeException("No Such Local Variable");
        else {
            Tuple<Integer,Boolean> info = locals.get(name);
            //load adress of local on stack
            cg.emit.emit("leal",info.a + "(%ebp)",targetReg);
            //if reference type: dereference
            /*if(info.b){
                cg.emit.emitMove("(" + targetReg + ")",targetReg);
            }*/
        }
    }

    public void loadThisRefInRegister(Register targetReg){
            //load adress of this reference (Memory adress, not stack adress => movl)
            cg.emit.emit("movl","8(%ebp)",targetReg);
    }

    public void loadLocalVarValueInRegister(String name, Register targetReg){

        if (!locals.containsKey(name)) throw new RuntimeException("No Such Local Variable");
        else {
            Tuple<Integer,Boolean> info = locals.get(name);
            //load adress of local on stack
            cg.emit.emit("movl",info.a + "(%ebp)",targetReg);
            //if reference type: dereference
            /*if(info.b){
                cg.emit.emitMove("(" + targetReg + ")",targetReg);
            }*/
        }
    }

    public boolean isLocal(String varName){
        return locals.containsKey(varName);
    }

    public void loadArgAdressInRegister(String name, Register targetReg){

        if (!args.containsKey(name)) throw new RuntimeException("No Such Argument");
        else {
            Tuple<Integer,Boolean> info = args.get(name);
            //load adress of arg on stack
            cg.emit.emit("leal",info.a + "(%ebp)",targetReg);
            //if reference type: dereference
            /*if(info.b){
                cg.emit.emitMove("(" + targetReg + ")",targetReg);
            }*/
        }
    }

    public void loadArgValueInRegister(String name, Register targetReg){

        if (!args.containsKey(name)) throw new RuntimeException("No Such Argument");
        else {
            Tuple<Integer,Boolean> info = args.get(name);
            //load adress of arg on stack
            cg.emit.emit("movl",info.a + "(%ebp)",targetReg);
            //if reference type: dereference
            /*if(info.b){
                cg.emit.emitMove("(" + targetReg + ")",targetReg);
            }*/
        }
    }

    //return to caller
    //adapt stack pointers and move to return adress
    public void ret(Ast.Expr arg){
        storeAllUsedRegisters();
        //get return value if any
        if (arg != null) {
            UUID regId = cg.eg.visit(arg, this);
            Register reg = getRegisterFromId(regId);
            if (arg.type.isReferenceType()) {
                //pass by value
                reg = Util.copyObject(reg);
            }
            //return value in eax
            cg.emit.emitMove(reg, Register.EAX);
            //all registers should be freed after return
            storeAllUsedRegisters();
        }
        else cg.emit.emit("movl", "$0", Register.EAX);
        cg.emit.emit("leal","4(" + Register.EBP + ")", Register.ESP);
        cg.emit.emitMove("(" + Register.EBP + ")", Register.EBP);
        cg.emit.emitRaw("ret");
    }

    public void call(Ast.MethodCall callee){

        //free all registers
        this.storeAllUsedRegisters();

        //copy and push params on stack
        for (Ast.Expr ast : callee.getMethodCallExpr().argumentsWithoutReceiver()){
            UUID regId = cg.eg.visit(ast, this);
            Register reg = getRegisterFromId(regId);
            //copy to pass by value if reference type:
            if (ast.type.isReferenceType()){
                reg = copyObject(regId,ast);//this is safe because at this point only one register (reg) is in use
                // TODO: cg.emit.emitMove("(" + reg + ")",reg); needed?
            }
            //easier if expanded type:
            cg.emit.emit("pushl", reg);
            releaseRegister(regId);
        }

        //push this reference on stack
        UUID regId = cg.eg.visit(callee.getMethodCallExpr().receiver(),this);
        Register reg = getRegisterFromId(regId);
        cg.emit.emit("pushl", reg);

        //call the function: automatically pushes return adress!
        cg.emit.emitMove("(" + reg + ")",reg);
        cg.emit.emitMove(callee.getMethodCallExpr().sym.offset + "(" + reg  + ")",reg);
        cg.emit.emitRaw("call *" + reg);
        //TODO (Kevin) dynamic binding => look actual method (tag/adress) up!!!

        //after returned from the call pop args of the stack (so that the offset in register spilling doesn't get confused)
        for (Ast.Expr ast : callee.getMethodCallExpr().argumentsWithoutReceiver()){
            cg.emit.emitRaw("popl %ebx");
        }
    }

    public Register call(Ast.MethodCallExpr callee){

        //free all registers
        this.storeAllUsedRegisters();

        //copy and push params on stack
        for (Ast.Expr ast : callee.argumentsWithoutReceiver()){
            UUID regId = cg.eg.visit(ast, this);
            Register reg = getRegisterFromId(regId);
            //copy to pass by value if reference type:
            if (ast.type.isReferenceType()){
                reg = copyObject(regId, ast);//this is safe because at this point only one register (reg) is in use
            }
            //easier if expanded type:
            cg.emit.emit("pushl", reg);
            releaseRegister(regId);
        }
        //push this reference on stack
        UUID regId = cg.eg.visit(callee.receiver(), this);
        Register reg = getRegisterFromId(regId);
        cg.emit.emit("pushl",reg);

        //call the function: automatically pushes return adress!
        cg.emit.emitMove("(" + reg + ")",reg);
        cg.emit.emitMove(callee.sym.offset + "(" + reg  + ")",reg);
        cg.emit.emitRaw("call *" + reg);

        //after returned from the call pop args of the stack (so that the offset in register spilling doesn't get confused)
        for (Ast.Expr ast : callee.argumentsWithoutReceiver()){
            cg.emit.emitRaw("popl %ebx");
        }
        //all registers are free, so always use eax for return value
        return Register.EAX;
    }

    private Register copyObject(UUID regId, Ast.Expr ast) {

        clearRegister(Register.EAX);
        clearRegister(Register.EBX);

        Register reg = getRegisterFromId(regId);

        int size = Util.classSizeMap.get(ast.type.name);
        cg.emit.emit("sub", constant(16), STACK_REG);
        cg.emit.emitStore(constant(size), 0, STACK_REG);
        cg.emit.emit("call", Config.CALLOC);
        cg.emit.emit("add", constant(16), STACK_REG);

        cg.emit.emit("leal","(" + Config.CLASS + ast.type + ")", RegisterManager.Register.EBX);
        cg.emit.emitMove(RegisterManager.Register.EBX, "(" + RegisterManager.Register.EAX + ")");

        for (int i = 4;i < size;i+=4){
            cg.emit.emitMove(i + "(" + reg + ")",Register.EBX);
            cg.emit.emitMove(Register.EBX, i + "(%eax)");
        }
        return Register.EAX;
    }

    public boolean isArg(String argName){
        return args.containsKey(argName);
    }
}
