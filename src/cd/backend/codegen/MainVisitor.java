package cd.backend.codegen;

import cd.Config;
import cd.ir.Ast;
import cd.ir.AstVisitor;
import cd.util.Util;

import java.util.UUID;

import static cd.backend.codegen.AssemblyEmitter.constant;
import static cd.backend.codegen.RegisterManager.STACK_REG;

/**
 * Created by kevin on 03.12.15.
 */
public class MainVisitor extends AstVisitor<Void,String> {

    private AstCodeGenerator astCodeGenerator;

    public MainVisitor(AstCodeGenerator astCodeGenerator){
        this.astCodeGenerator = astCodeGenerator;
    }

    @Override
    public Void classDecl(Ast.ClassDecl ast, String arg) {

        if (!ast.name.equals("Main")) return null;

        astCodeGenerator.emit.emitRaw(Config.TEXT_SECTION);
        astCodeGenerator.emit.emitRaw(".globl main");
        astCodeGenerator.emit.emitLabel("main");

        // allocate memory for Main instance
        astCodeGenerator.emit.emit("sub", constant(16), STACK_REG);
        astCodeGenerator.emit.emitStore(constant(Util.sizeOf(ast)), 0, STACK_REG);
        astCodeGenerator.emit.emit("call", Config.CALLOC);
        astCodeGenerator.emit.emit("add", constant(16), STACK_REG);

        // put VTable at the beginning of the allocated space
        astCodeGenerator.emit.emit("leal", "(" + Config.CLASS + "Main" + ")", RegisterManager.Register.EBX);
        astCodeGenerator.emit.emitMove(RegisterManager.Register.EBX, "(" + RegisterManager.Register.EAX + ")");
        //astCodeGenerator.emit.emitStore(RegisterManager.Register.ESP,4, RegisterManager.Register.EAX);
        //astCodeGenerator.emit.emitMove(RegisterManager.Register.EBX, "(" + RegisterManager.Register.EAX + ")");

        // push the Main instance on the stack as if it was the this object
        astCodeGenerator.emit.emit("pushl", RegisterManager.Register.EAX);

        // call the main method
        astCodeGenerator.emit.emit("call", "Main_main");
        astCodeGenerator.emit.emit("pushl", "$0");
        astCodeGenerator.emit.emit("call", "exit");

        return null;

    }

}
