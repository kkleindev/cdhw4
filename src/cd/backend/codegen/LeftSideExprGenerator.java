package cd.backend.codegen;

import cd.ir.Ast;
import cd.ir.Symbol;

import java.util.UUID;

/**
 * Created by felix on 04/12/15.
 */
public class LeftSideExprGenerator extends ExprGenerator {
    LeftSideExprGenerator(AstCodeGenerator astCodeGenerator) {
        super(astCodeGenerator);
    }

    @Override
    public UUID index(Ast.Index ast,StackManager stackManager) {
        UUID indexId = cg.eg.visit(ast.right(),stackManager);
        RegisterManager.Register reg = stackManager.getRegisterFromId(indexId);
        UUID targetId = cg.eg.visit(ast.left(),stackManager);
        RegisterManager.Register targetReg = stackManager.getRegisterFromId(targetId);

        //cg.emit.emit("imul", "$4", reg);
        cg.emit.emit("leal", "(" + targetReg +  ", " + reg + ", 4)",targetReg);

        stackManager.releaseRegister(indexId);
        return targetId;
    }

    @Override
    public UUID field(Ast.Field ast,StackManager stackManager) {
        int offset = 0;
        UUID id = cg.eg.visit(ast.arg(), stackManager);
        RegisterManager.Register fieldReg = stackManager.getRegisterFromId(id);
        Symbol.ClassSymbol classSymbol = (Symbol.ClassSymbol) ast.arg().type;
        offset = classSymbol.fieldOffsets.get(ast.fieldName);
        cg.emit.emit("leal", offset + "(" + fieldReg + ")", fieldReg);
        return id;
    }

    @Override
    public UUID var(Ast.Var ast,StackManager stackManager) {
        UUID regId = stackManager.getId();
        RegisterManager.Register reg = stackManager.getRegisterFromId(regId);
        if(ast.sym.kind == Symbol.VariableSymbol.Kind.LOCAL){
            stackManager.loadLocalVarAdressInRegister(ast.name, reg);
        }
        else if(ast.sym.kind == Symbol.VariableSymbol.Kind.PARAM){
            stackManager.loadArgAdressInRegister(ast.name, reg);
        }
        else throw new RuntimeException("blubb");
        return regId;
    }
}
