package cd.backend.codegen;

import static cd.Config.SCANF;
import static cd.backend.codegen.AssemblyEmitter.constant;
import static cd.backend.codegen.RegisterManager.STACK_REG;

import cd.Config;
import cd.ToDoException;
import cd.backend.codegen.RegisterManager.Register;
import cd.ir.Ast.BinaryOp;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.BuiltInRead;
import cd.ir.Ast.Cast;
import cd.ir.Ast.Expr;
import cd.ir.Ast.Field;
import cd.ir.Ast.Index;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.MethodCallExpr;
import cd.ir.Ast.NewArray;
import cd.ir.Ast.NewObject;
import cd.ir.Ast.NullConst;
import cd.ir.Ast.ThisRef;
import cd.ir.Ast.UnaryOp;
import cd.ir.Ast.Var;
import cd.ir.ExprVisitor;
import cd.ir.Symbol;
import cd.util.Util;
import cd.util.debug.AstOneLine;

import java.util.UUID;

/**
 * Generates code to evaluate expressions. After emitting the code, returns a
 * UUID where the result can be found.
 */
class ExprGenerator extends ExprVisitor<UUID,StackManager> {
	protected final AstCodeGenerator cg;

	ExprGenerator(AstCodeGenerator astCodeGenerator) {
		cg = astCodeGenerator;
	}

	public UUID gen(Expr ast) {
		return visit(ast, null);
	}

	@Override
	public UUID visit(Expr ast,StackManager stackManager) {
		try {
			cg.emit.increaseIndent("Emitting " + AstOneLine.toString(ast));
			return super.visit(ast, stackManager);
		} finally {
			cg.emit.decreaseIndent();
		}

	}

	@Override
	public UUID binaryOp(BinaryOp ast,StackManager stackManager) {
		// Simplistic HW1 implementation that does
		// not care if it runs out of registers, and
		// supports only a limited range of operations:
		String endLabel = cg.emit.uniqueLabel();
		String elseLabel = cg.emit.uniqueLabel();
		String op = "";
		boolean comparison = false;

		int leftRN = cg.rnv.calc(ast.left());
		int rightRN = cg.rnv.calc(ast.right());

		UUID leftRegId, rightRegId;
		if (leftRN > rightRN) {
			leftRegId = visit(ast.left(), stackManager);
			rightRegId = visit(ast.right(), stackManager);
		} else {
			rightRegId = visit(ast.right(), stackManager);
			leftRegId = visit(ast.left(), stackManager);
		}
		Register leftReg = stackManager.getRegisterFromId(leftRegId);
		Register rightReg = stackManager.getRegisterFromId(rightRegId);

		cg.debug("Binary Op: %s (%s,%s)", ast, leftReg, rightReg);

		switch (ast.operator) {

			case B_TIMES:
				cg.emit.emit("imul", rightReg, leftReg);
				break;

			case B_PLUS:
				cg.emit.emit("add", rightReg, leftReg);
				break;

			case B_MINUS:
				cg.emit.emit("sub", rightReg, leftReg);
				break;

			case B_GREATER_OR_EQUAL:
				comparison = true;
				op = "jge";
				break;

			case B_AND:
				cg.emit.emit("and",rightReg,leftReg);
				break;

			case B_GREATER_THAN:
				comparison = true;
				op = "jg";
				break;

			case B_DIV:
				break;

			case B_EQUAL:
				comparison = true;
				op = "je";
				break;

			case B_LESS_OR_EQUAL:
				comparison = true;
				op = "jle";
				break;

			case B_LESS_THAN:
				comparison = true;
				op = "jl";
				break;

			case B_MOD:
				break;

			case B_NOT_EQUAL:
				comparison = true;
				op = "jne";
				break;

			case B_OR:
				cg.emit.emit("or",rightReg,leftReg);
				break;

			default:
				throw new ToDoException();
		}
		if (comparison){
			cg.emit.emit("cmp", rightReg, leftReg);
			cg.emit.emit(op, elseLabel);
			cg.emit.emitMove(AssemblyEmitter.constant(0), leftReg);
			cg.emit.emit("jmp", endLabel);
			cg.emit.emitRaw(elseLabel + ":");
			cg.emit.emitMove(AssemblyEmitter.constant(1), leftReg);
			cg.emit.emitRaw(endLabel + ":");
		}

		stackManager.releaseRegister(rightRegId);

		return leftRegId;
	}

	@Override
	public UUID booleanConst(BooleanConst ast,StackManager stackManager) {
		UUID regId = stackManager.getId();
		Register reg = stackManager.getRegisterFromId(regId);
		int i = 0;
		if (ast.value) i = 1;
		cg.emit.emitMove(AssemblyEmitter.constant(i),reg);
		return regId;
	}

	@Override
	public UUID builtInRead(BuiltInRead ast,StackManager stackManager) {
		UUID regId = stackManager.getId();
		Register reg = stackManager.getRegisterFromId(regId);

		cg.emit.emit("sub", constant(16), STACK_REG);
		cg.emit.emit("leal", AssemblyEmitter.registerOffset(8, STACK_REG), reg);
		cg.emit.emitStore(reg, 4, STACK_REG);
		cg.emit.emitStore("$STR_D", 0, STACK_REG);
		cg.emit.emit("call", SCANF);
		cg.emit.emitLoad(8, STACK_REG, reg);
		cg.emit.emit("add", constant(16), STACK_REG);

		return regId;
	}

	@Override
	public UUID cast(Cast ast,StackManager stackManager) {
		{
			throw new ToDoException();
		}
	}

	@Override
	public UUID index(Index ast,StackManager stackManager) {
		UUID indexId = visit(ast.right(),stackManager);
		Register reg = stackManager.getRegisterFromId(indexId);
		UUID targetId = visit(ast.left(),stackManager);
		Register targetReg = stackManager.getRegisterFromId(targetId);

		//cg.emit.emit("imul", "$4", reg);
		cg.emit.emitMove("(" + targetReg +  ", " + reg + ", 4)",targetReg);

		stackManager.releaseRegister(indexId);
		return targetId;
	}

	@Override
	public UUID intConst(IntConst ast,StackManager stackManager) {
		{
			UUID regId = stackManager.getId();
			Register reg = stackManager.getRegisterFromId(regId);
			cg.emit.emit("movl", "$" + ast.value, reg);
			return regId;
		}
	}

	@Override

	public UUID field(Field ast,StackManager stackManager) {

		int offset = 0;
		UUID id = visit(ast.arg(),stackManager);
		Register fieldReg = stackManager.getRegisterFromId(id);
		Symbol.ClassSymbol classSymbol = (Symbol.ClassSymbol) ast.arg().type;
		offset = classSymbol.fieldOffsets.get(ast.fieldName);
		cg.emit.emit("leal",offset + "(" + fieldReg + ")",fieldReg);
		cg.emit.emitMove("(" + fieldReg + ")", fieldReg);
		return id;
	}

	@Override
	public UUID newArray(NewArray ast,StackManager stackManager) {

		UUID sizeId = visit(ast.arg(), stackManager);
		Register sizeReg = stackManager.getRegisterFromId(sizeId);

		stackManager.clearRegister(Register.EAX);
		stackManager.clearRegister(Register.EBX);

		cg.emit.emit("sub", constant(16), STACK_REG);
		cg.emit.emit("imul", "$4", sizeReg);
		cg.emit.emitMove(sizeReg,"(" + STACK_REG + ")");
		cg.emit.emit("call", Config.CALLOC);
		cg.emit.emit("add", constant(16), STACK_REG);

		cg.emit.emitMove(Register.EAX,sizeReg);

		return sizeId;
	}

	@Override
	public UUID newObject(NewObject ast,StackManager stackManager) {
		Symbol.ClassSymbol classSymbol = (Symbol.ClassSymbol) ast.type;

		stackManager.clearRegister(Register.EAX);
		stackManager.clearRegister(Register.EBX);

		cg.emit.emit("sub", constant(16), STACK_REG);
		cg.emit.emitStore(constant(Util.sizeOf(classSymbol.ast)), 0, STACK_REG);
		cg.emit.emit("call", Config.CALLOC);
		cg.emit.emit("add", constant(16), STACK_REG);

		cg.emit.emit("leal", "(" + Config.CLASS + ast.type + ")", RegisterManager.Register.EBX);
		cg.emit.emitMove(RegisterManager.Register.EBX, "(" + RegisterManager.Register.EAX + ")");

		UUID id = stackManager.getId();
		Register reg = stackManager.getRegisterFromId(id);
		cg.emit.emitMove(Register.EAX,reg);

		return id;
	}

	@Override
	public UUID nullConst(NullConst ast,StackManager stackManager) {
		UUID id = stackManager.getId();
		Register reg = stackManager.getRegisterFromId(id);
		cg.emit.emitMove(AssemblyEmitter.constant(0),reg);
		return id;
	}

	@Override
	public UUID thisRef(ThisRef ast,StackManager stackManager) {

		UUID regId = stackManager.getId();
		Register reg = stackManager.getRegisterFromId(regId);
		//loads the adress of this object into reg
		stackManager.loadThisRefInRegister(reg);
		return regId;
	}

	@Override
	public UUID methodCall(MethodCallExpr ast,StackManager stackManager) {
			Register reg = stackManager.call(ast);
			//carefull! reg should be eax but is free!
			UUID newRegId = stackManager.getId();
			Register newReg = stackManager.getRegisterFromId(newRegId);
			cg.emit.emitMove(reg,newReg);
			//no need to free reg since it was never 'taken'
			return newRegId;
	}

	@Override
	public UUID unaryOp(UnaryOp ast,StackManager stackManager) {
		{
			UUID argRegId = visit(ast.arg(), stackManager);
			Register stackManagerReg = stackManager.getRegisterFromId(argRegId);
			switch (ast.operator) {
			case U_PLUS:
				break;

			case U_MINUS:
				cg.emit.emit("negl", stackManagerReg);
				break;

			case U_BOOL_NOT:
				cg.emit.emit("negl", stackManagerReg);
				cg.emit.emit("incl", stackManagerReg);
				break;
			}

			return argRegId;
		}
	}
	
	@Override
	public UUID var(Var ast,StackManager stackManager) {
		UUID regId = stackManager.getId();
		Register reg = stackManager.getRegisterFromId(regId);
		if(ast.sym.kind == Symbol.VariableSymbol.Kind.LOCAL){
			stackManager.loadLocalVarValueInRegister(ast.name, reg);
		}
		else if(ast.sym.kind == Symbol.VariableSymbol.Kind.PARAM){
			stackManager.loadArgValueInRegister(ast.name,reg);
		}
		else {
			cg.emit.emit("movl", ast.sym.assemblyTag, reg);
		}
		return regId;
	}

}
