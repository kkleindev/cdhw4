package cd.backend.codegen;

import static cd.Config.MAIN;
import static cd.backend.codegen.AssemblyEmitter.constant;
import static cd.backend.codegen.RegisterManager.STACK_REG;

import java.util.List;
import java.util.UUID;

import cd.Config;
import cd.ToDoException;
import cd.backend.codegen.RegisterManager.Register;
import cd.ir.Ast;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BuiltInWrite;
import cd.ir.Ast.BuiltInWriteln;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.Expr;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.ReturnStmt;
import cd.ir.Ast.Var;
import cd.ir.Ast.VarDecl;
import cd.ir.Ast.WhileLoop;
import cd.ir.AstVisitor;
import cd.ir.Symbol.MethodSymbol;
import cd.util.debug.AstOneLine;

/**
 * Generates code to process statements and declarations.
 */
class StmtGenerator extends AstVisitor<UUID, StackManager> {
	protected final AstCodeGenerator cg;

	StmtGenerator(AstCodeGenerator astCodeGenerator) {
		cg = astCodeGenerator;
	}

	public void gen(Ast ast) {
		visit(ast, null);
	}

	@Override
	public UUID visit(Ast ast, StackManager stackManager) {
		try {
			cg.emit.increaseIndent("Emitting " + AstOneLine.toString(ast));
			return super.visit(ast, stackManager);
		} finally {
			cg.emit.decreaseIndent();
		}
	}

	@Override
	public UUID methodCall(MethodCall ast, StackManager dummy) {
		{
			dummy.call(ast);
			return null;
		}
	}

	public UUID methodCall(MethodSymbol sym, List<Expr> allArguments) {
		throw new RuntimeException("Not required");
	}

	// Emit vtable for arrays of this class:
	@Override
	public UUID classDecl(ClassDecl ast, StackManager stackManager) {
		return visitChildren(ast, stackManager);
	}

	@Override
	public UUID methodDecl(MethodDecl ast, StackManager stackManager) {

	    /*Stack (grows downwards):
    1. old frame Pointer (offset 0) (done by caller)
    2. local Variables (offset -4 - ...)
    3. spilled registers
    4. parameters (function arguments) (caller)
    5. return adress (caller)
     */

		// ------------------------------------------------------------
		// Homework 1 Prologue Generation:
		// Rather simplistic due to limited requirements!

		/*
		if (ast.name.equals("main")) {



			// Emit the main() method:
			cg.emit.emitRaw(Config.TEXT_SECTION);
			cg.emit.emitRaw(".globl " + MAIN);
			cg.emit.emitLabel(MAIN);

		}
		//emit tag
		//cg.emit.emitRaw(ast.sym.assemblyTag + ":");
		*/

		//mostly handled by stack manager
		cg.emit.emitRaw(ast.sym.assemblyTag + ":");
		stackManager = new StackManager(ast, this.cg);
		visit(ast.body(),stackManager);
		if (ast.returnType.equals("void")){
			stackManager.ret(null);
		}
		return null;
	}

	@Override
	public UUID ifElse(IfElse ast, StackManager stackManager) {
		{
			String endLabel = cg.emit.uniqueLabel();
			String elseLabel = cg.emit.uniqueLabel();
			UUID regConditionId = cg.eg.visit(ast.condition(), stackManager);
			Register regCondition = stackManager.getRegisterFromId(regConditionId);
			//TODO comparison assuming false = 0
			UUID compareRegId = stackManager.getId();
			Register compareReg = stackManager.getRegisterFromId(compareRegId);
			cg.emit.emitMove("$1",compareReg);
			cg.emit.emit("cmpl", regCondition, compareReg);
			stackManager.releaseRegister(compareRegId);
			//has else statement
			if (ast.otherwise() != null) {
				cg.emit.emit("jg", elseLabel);
				visit(ast.then(), stackManager);
				cg.emit.emitRaw("jmp " + endLabel);
				cg.emit.emitRaw(elseLabel + ":");
				visit(ast.otherwise(), stackManager);
				cg.emit.emitRaw(endLabel + ":");

			}
			//has no else statement
			else {
				cg.emit.emit("JL", endLabel);
				visit(ast.then(),stackManager);
				cg.emit.emitRaw(endLabel + ":");
			}
			return null;
		}
	}

	@Override
	public UUID whileLoop(WhileLoop ast, StackManager stackManager) {
		{
			String beginLable = cg.emit.uniqueLabel();
			String exitLabel = cg.emit.uniqueLabel();
			UUID regConditionId = cg.eg.visit(ast.condition(), stackManager);
			Register regCondition = stackManager.getRegisterFromId(regConditionId);
			//TODO comparison assuming false = 0
			UUID compareRegId = stackManager.getId();
			Register compareReg = stackManager.getRegisterFromId(compareRegId);
			cg.emit.emitMove("$1", compareReg);
			cg.emit.emit("cmpl", regCondition, compareReg);
			cg.emit.emit("jg", exitLabel);
			cg.emit.emitRaw(beginLable + ":");
			//loop body

			stackManager.releaseRegister(regConditionId);
			//at this point no register should be in use
			visit(ast.body(),stackManager);

			//condition
			regConditionId = cg.eg.visit(ast.condition(),stackManager);
			regCondition = stackManager.getRegisterFromId(regConditionId);
			cg.emit.emit("cmp",regCondition,compareReg);
			cg.emit.emit("jg", exitLabel);
			cg.emit.emit("jmp", beginLable);

			//exit
			cg.emit.emitRaw(exitLabel + ":");
			stackManager.releaseRegister(compareRegId);
			return null;
		}
	}

	@Override
	public UUID assign(Assign ast, StackManager stackManager) {
		UUID lhsRegId = cg.lsg.visit(ast.left(),stackManager);
		Register lhsReg = stackManager.getRegisterFromId(lhsRegId);
		UUID rhsRegId = cg.eg.visit(ast.right(), stackManager);
		Register rhsReg = stackManager.getRegisterFromId(rhsRegId);
		cg.emit.emitMove(rhsReg,"(" + lhsReg + ")");
		stackManager.releaseRegister(lhsRegId);
		/*if (!(ast.left() instanceof Var)){
			UUID lhsRegId = cg.eg.visit(ast.left(),stackManager);
			Register lhsReg = stackManager.getRegisterFromId(lhsRegId);
			cg.emit.emitMove(rhsReg,"(" + lhsReg + ")");
			stackManager.releaseRegister(lhsRegId);
		}
		else if (stackManager.isLocal(((Var) ast.left()).name)){
			UUID lhsRegId = stackManager.getId();
			Register lhsReg = stackManager.getRegisterFromId(lhsRegId);
			stackManager.loadLocalVarAdressInRegister(((Var) ast.left()).name,lhsReg);
			cg.emit.emitMove(rhsReg,"(" + lhsReg + ")");
			stackManager.releaseRegister(lhsRegId);
		}
		else{
			Var var = (Var) ast.left();
			cg.emit.emit("movl", rhsReg, AstCodeGenerator.VAR_PREFIX + var.name);
		}*/
		stackManager.releaseRegister(rhsRegId);
		return null;
	}

	@Override
	public UUID builtInWrite(BuiltInWrite ast, StackManager stackManager) {
		UUID regId = cg.eg.visit(ast.arg(), stackManager);
		Register reg = stackManager.getRegisterFromId(regId);
		cg.emit.emit("sub", constant(16), STACK_REG);
		cg.emit.emitStore(reg, 4, STACK_REG);
		cg.emit.emitStore("$STR_D", 0, STACK_REG);
		cg.emit.emit("call", Config.PRINTF);
		cg.emit.emit("add", constant(16), STACK_REG);
		stackManager.releaseRegister(regId);
		return null;
	}

	@Override
	public UUID builtInWriteln(BuiltInWriteln ast, StackManager stackManager) {
		cg.emit.emit("sub", constant(16), STACK_REG);
		cg.emit.emitStore("$STR_NL", 0, STACK_REG);
		cg.emit.emit("call", Config.PRINTF);
		cg.emit.emit("add", constant(16), STACK_REG);
		return null;
	}

	@Override
	public UUID returnStmt(ReturnStmt ast, StackManager stackManager) {
		{
			stackManager.ret(ast.arg());
			return null;
		}
	}

}
