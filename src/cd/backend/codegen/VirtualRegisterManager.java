package cd.backend.codegen;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;

import cd.backend.codegen.RegisterManager.*;

/**
 * Created by felix on 17/11/15.
 */
public class VirtualRegisterManager {
    /*
    One Virtual RegisterManager per MethodCall -> One region of the Stack.
    Only Offset needs to be known if stack pointers are implemented corrected.
    Functionality :
    Trouble cases:
    1.Single statement uses too many registers. -> explicit spilling
    2.Method gets called while registers are in use -> always push all registers on stack before a (not build in) method call
    (3.Registers kept over multiple statements -> unneccessary (if neglecting performance), push to stack or remove code that
    uses these registers)
    */

    private Queue<Register> registerQueue;
    public AstCodeGenerator cg;
    //mapping to find the virtual register that represents an actual register
    private HashMap<Register,UUID> registerHashMap;

    // offset (in bytes) for the next item on stack
    public int offset = 0;

    //mapping form ID to virtual register
    private HashMap<UUID,VirtualRegister> virtualRegisterHashMap;

    public VirtualRegisterManager(AstCodeGenerator cg){
        this.cg = cg;

        registerQueue = new LinkedList<Register>();
        //Initialize registers
        registerHashMap = new HashMap<>();
        for (Register reg:RegisterManager.GPR){
            registerHashMap.put(reg,null);
        }

        virtualRegisterHashMap = new HashMap<>();
    }

    public void lowerOffsetBy(int i){
        offset-=i;
    }

    public void storeAllUsedRegisters(){
        for (Register reg:RegisterManager.GPR){
            UUID id = registerHashMap.get(reg);
            if (id != null){
                clearRegister(reg);
            }
        }
    }

    public UUID getId(){
        UUID id = UUID.randomUUID();
        virtualRegisterHashMap.put(id,null);
        return id;
    }

    public Register getRegisterFromId(UUID id){

        //Case 1 : neither on stack nor direct Register
        if (virtualRegisterHashMap.get(id) == null){

            //Case 1.1 : there is an unused Register
            if (cg.rm.availableRegisters() > 0){
                Register reg = getNewRegister();
                virtualRegisterHashMap.put(id,new VirtualRegister(reg));
                registerHashMap.put(reg,id);
                return reg;
            }

            //Case 1.2 : There is no unused register
            else {
                clearRegister();
                Register reg =  getNewRegister();
                virtualRegisterHashMap.put(id,new VirtualRegister(reg));
                registerHashMap.put(reg,id);
                return reg;
            }
        }

        //Case 2 : is real register
        else if(virtualRegisterHashMap.get(id).isRegister()){
            return virtualRegisterHashMap.get(id).register;
        }

        //Case 3 : is on Stack
        else {
            Register reg = getRegister();
            int off = virtualRegisterHashMap.get(id).getOffset();
            emitLoad(reg,off);
            virtualRegisterHashMap.get(id).loaded(reg);
            registerHashMap.put(reg,id);
            return reg;
        }
    }

    private Register getNewRegister() {
        Register reg = cg.rm.getRegister();
        registerQueue.add(reg);
        return reg;
    }

    private void emitLoad(Register reg, int off) {
        cg.emit.emit("movl",off + "(%ebp)",reg);
    }

    private Register getRegister(){
        clearRegister();
        return getNewRegister();
    }

    
    public void releaseRegister(UUID regId){
        VirtualRegister vr = virtualRegisterHashMap.get(regId);
        //remove entry from mapping
        virtualRegisterHashMap.remove(regId);
        if (vr.isRegister()){
            //update registerHashmap
            registerHashMap.put(vr.getRegister(),null);
            //release register
            cg.rm.releaseRegister(vr.getRegister());
        }
    }

    //clear random register
    private void clearRegister(){
        if (cg.rm.availableRegisters() == 0) {
            Register reg = registerQueue.remove(); //register to be cleared
            virtualRegisterHashMap.get(registerHashMap.get(reg)).push(offset);
            registerHashMap.put(reg, null);
            emitPush(reg);
            cg.rm.releaseRegister(reg);
        }
    }

    //clear defined register
    public void clearRegister(Register reg){
        if (registerHashMap.get(reg) != null){
            virtualRegisterHashMap.get(registerHashMap.get(reg)).push(offset);
            registerHashMap.put(reg, null);
            emitPush(reg);
            cg.rm.releaseRegister(reg);
        }
    }

    private void emitPush(Register reg) {
        cg.emit.emit("pushl",reg);
        lowerOffsetBy(4);
    }

    private class VirtualRegister {

        // if virtual Register is mapped to real register
        private Register register;

        // if virtual Register is mapped to heap
        private int offset;

        public VirtualRegister(Register register){
            this.register = register;
        }

        boolean isRegister(){
            return (register != null);
        }

        public void push(int offset){
            register = null;
            this.offset = offset;
        }

        public void loaded(Register reg){
            register = reg;
        }

        public Register getRegister(){
            return register;
        }

        public int getOffset() {
            return offset;
        }
    }
}
