package cd.backend.codegen;

import cd.ir.Ast;
import cd.ir.AstVisitor;
import cd.ir.Symbol;
import cd.util.Util;

/**
 * Created by felix on 02/12/15.
 */
public class AssemblyTagVisitor extends AstVisitor<Void,String> {

    @Override
    public Void classDecl(Ast.ClassDecl ast, String arg) {
        arg = ast.name;
        visitChildren(ast, arg);
        return null;
    }

    @Override
    public Void methodCall(Ast.MethodCall ast, String arg) {
        StringBuilder sb = new StringBuilder();
        sb.append(ast.getMethodCallExpr().receiver().type + "/" + ast.getMethodCallExpr().methodName);
        for (Ast.Expr expr : ast.getMethodCallExpr().argumentsWithoutReceiver()){
            sb.append("/" + expr.type);
        }
        ast.getMethodCallExpr().sym.assemblyTag = sb.toString();
        return null;
    }

    @Override
    public Void varDecl(Ast.VarDecl ast, String arg) {
        ast.sym.assemblyTag = arg + "/" + ast.name;
        return null;
    }

    @Override
    public Void methodDecl(Ast.MethodDecl ast, String arg) {
        StringBuilder sb = new StringBuilder();
        sb.append(arg + "_" + ast.name);
        ast.sym.assemblyTag = sb.toString();
        visitChildren(ast,arg);
        return null;
    }

    @Override
    public Void methodCall(Ast.MethodCallExpr ast, String arg) {
        StringBuilder sb = new StringBuilder();
        sb.append(ast.receiver().type + "_" + ast.methodName);
        ast.sym.assemblyTag = sb.toString();
        return null;
    }
}

