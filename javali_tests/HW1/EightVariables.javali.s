.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting i0 = 0
          # Emitting i0
          leal -16(%ebp), %edi
          # Emitting 0
          movl $0, %esi
        movl %esi, (%edi)
        # Emitting i1 = 1
          # Emitting i1
          leal -20(%ebp), %esi
          # Emitting 1
          movl $1, %edi
        movl %edi, (%esi)
        # Emitting i2 = 2
          # Emitting i2
          leal -24(%ebp), %edi
          # Emitting 2
          movl $2, %esi
        movl %esi, (%edi)
        # Emitting i3 = 3
          # Emitting i3
          leal -28(%ebp), %esi
          # Emitting 3
          movl $3, %edi
        movl %edi, (%esi)
        # Emitting i4 = 4
          # Emitting i4
          leal -32(%ebp), %edi
          # Emitting 4
          movl $4, %esi
        movl %esi, (%edi)
        # Emitting i5 = 5
          # Emitting i5
          leal -36(%ebp), %esi
          # Emitting 5
          movl $5, %edi
        movl %edi, (%esi)
        # Emitting i6 = 6
          # Emitting i6
          leal -40(%ebp), %edi
          # Emitting 6
          movl $6, %esi
        movl %esi, (%edi)
        # Emitting i7 = 7
          # Emitting i7
          leal -44(%ebp), %esi
          # Emitting 7
          movl $7, %edi
        movl %edi, (%esi)
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
