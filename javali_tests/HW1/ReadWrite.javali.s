.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting i0 = 5
          # Emitting i0
          leal -12(%ebp), %edi
          # Emitting 5
          movl $5, %esi
        movl %esi, (%edi)
        # Emitting i1 = read()
          # Emitting i1
          leal -16(%ebp), %esi
          # Emitting read()
          sub $16, %esp
          leal 8(%esp), %edi
          movl %edi, 4(%esp)
          movl $STR_D, 0(%esp)
          call scanf
          movl 8(%esp), %edi
          add $16, %esp
        movl %edi, (%esi)
        # Emitting r1 = (i0 + i1)
          # Emitting r1
          leal -4(%ebp), %edi
          # Emitting (i0 + i1)
            # Emitting i1
            movl -16(%ebp), %esi
            # Emitting i0
            movl -12(%ebp), %edx
          add %esi, %edx
        movl %edx, (%edi)
        # Emitting write(r1)
          # Emitting r1
          movl -4(%ebp), %edx
        sub $16, %esp
        movl %edx, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting write((r1 - 3))
          # Emitting (r1 - 3)
            # Emitting 3
            movl $3, %edx
            # Emitting r1
            movl -4(%ebp), %edi
          sub %edx, %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
