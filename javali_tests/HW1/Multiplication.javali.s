.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting i0 = 5
          # Emitting i0
          leal -8(%ebp), %edi
          # Emitting 5
          movl $5, %esi
        movl %esi, (%edi)
        # Emitting i1 = 2
          # Emitting i1
          leal -12(%ebp), %esi
          # Emitting 2
          movl $2, %edi
        movl %edi, (%esi)
        # Emitting r1 = (i1 * 3)
          # Emitting r1
          leal -4(%ebp), %edi
          # Emitting (i1 * 3)
            # Emitting 3
            movl $3, %esi
            # Emitting i1
            movl -12(%ebp), %edx
          imul %esi, %edx
        movl %edx, (%edi)
        # Emitting write(r1)
          # Emitting r1
          movl -4(%ebp), %edx
        sub $16, %esp
        movl %edx, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting r1 = (i0 * i1)
          # Emitting r1
          leal -4(%ebp), %edx
          # Emitting (i0 * i1)
            # Emitting i1
            movl -12(%ebp), %edi
            # Emitting i0
            movl -8(%ebp), %esi
          imul %edi, %esi
        movl %esi, (%edx)
        # Emitting write(r1)
          # Emitting r1
          movl -4(%ebp), %esi
        sub $16, %esp
        movl %esi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting r1 = (((r1 * i0) * i1) * 3)
          # Emitting r1
          leal -4(%ebp), %esi
          # Emitting (((r1 * i0) * i1) * 3)
            # Emitting ((r1 * i0) * i1)
              # Emitting (r1 * i0)
                # Emitting i0
                movl -8(%ebp), %edx
                # Emitting r1
                movl -4(%ebp), %edi
              imul %edx, %edi
              # Emitting i1
              movl -12(%ebp), %edx
            imul %edx, %edi
            # Emitting 3
            movl $3, %edx
          imul %edx, %edi
        movl %edi, (%esi)
        # Emitting write(r1)
          # Emitting r1
          movl -4(%ebp), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
