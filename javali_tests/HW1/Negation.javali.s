.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting A = 1
          # Emitting A
          leal -4(%ebp), %edi
          # Emitting 1
          movl $1, %esi
        movl %esi, (%edi)
        # Emitting B = 1
          # Emitting B
          leal -8(%ebp), %esi
          # Emitting 1
          movl $1, %edi
        movl %edi, (%esi)
        # Emitting a = (A * -(B))
          # Emitting a
          leal -12(%ebp), %edi
          # Emitting (A * -(B))
            # Emitting -(B)
              # Emitting B
              movl -8(%ebp), %esi
            negl %esi
            # Emitting A
            movl -4(%ebp), %edx
          imul %esi, %edx
        movl %edx, (%edi)
        # Emitting b = (-(A) * B)
          # Emitting b
          leal -16(%ebp), %edx
          # Emitting (-(A) * B)
            # Emitting B
            movl -8(%ebp), %edi
            # Emitting -(A)
              # Emitting A
              movl -4(%ebp), %esi
            negl %esi
          imul %edi, %esi
        movl %esi, (%edx)
        # Emitting c = -((A + B))
          # Emitting c
          leal -20(%ebp), %esi
          # Emitting -((A + B))
            # Emitting (A + B)
              # Emitting B
              movl -8(%ebp), %edx
              # Emitting A
              movl -4(%ebp), %edi
            add %edx, %edi
          negl %edi
        movl %edi, (%esi)
        # Emitting d = -((A * B))
          # Emitting d
          leal -24(%ebp), %edi
          # Emitting -((A * B))
            # Emitting (A * B)
              # Emitting B
              movl -8(%ebp), %esi
              # Emitting A
              movl -4(%ebp), %edx
            imul %esi, %edx
          negl %edx
        movl %edx, (%edi)
        # Emitting write(a)
          # Emitting a
          movl -12(%ebp), %edx
        sub $16, %esp
        movl %edx, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting write(b)
          # Emitting b
          movl -16(%ebp), %edx
        sub $16, %esp
        movl %edx, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting write(c)
          # Emitting c
          movl -20(%ebp), %edx
        sub $16, %esp
        movl %edx, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting write(d)
          # Emitting d
          movl -24(%ebp), %edx
        sub $16, %esp
        movl %edx, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
