.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting i0 = 0
          # Emitting i0
          leal -4(%ebp), %edi
          # Emitting 0
          movl $0, %esi
        movl %esi, (%edi)
        # Emitting i0 = (5 + i0)
          # Emitting i0
          leal -4(%ebp), %esi
          # Emitting (5 + i0)
            # Emitting i0
            movl -4(%ebp), %edi
            # Emitting 5
            movl $5, %edx
          add %edi, %edx
        movl %edx, (%esi)
        # Emitting write(i0)
          # Emitting i0
          movl -4(%ebp), %edx
        sub $16, %esp
        movl %edx, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting i0 = (i0 + 5)
          # Emitting i0
          leal -4(%ebp), %edx
          # Emitting (i0 + 5)
            # Emitting 5
            movl $5, %esi
            # Emitting i0
            movl -4(%ebp), %edi
          add %esi, %edi
        movl %edi, (%edx)
        # Emitting write(i0)
          # Emitting i0
          movl -4(%ebp), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting i0 = ((i0 + 5) + 3)
          # Emitting i0
          leal -4(%ebp), %edi
          # Emitting ((i0 + 5) + 3)
            # Emitting (i0 + 5)
              # Emitting 5
              movl $5, %edx
              # Emitting i0
              movl -4(%ebp), %esi
            add %edx, %esi
            # Emitting 3
            movl $3, %edx
          add %edx, %esi
        movl %esi, (%edi)
        # Emitting write(i0)
          # Emitting i0
          movl -4(%ebp), %esi
        sub $16, %esp
        movl %esi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
