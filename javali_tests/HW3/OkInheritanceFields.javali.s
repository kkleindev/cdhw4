.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_A:
.int 0
CLASS_B:
.int CLASS_A
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class A {...}
    # Emitting int foo
  # Emitting class B {...}
    # Emitting int bar
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting a = new A()
          # Emitting a
          leal -4(%ebp), %edi
          # Emitting new A()
          sub $16, %esp
          movl $8, 0(%esp)
          call calloc
          add $16, %esp
          leal (CLASS_A), %ebx
          movl %ebx, (%eax)
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting a.foo = 1
          # Emitting a.foo
            # Emitting a
            movl -4(%ebp), %esi
          leal 4(%esi), %esi
          # Emitting 1
          movl $1, %edi
        movl %edi, (%esi)
        # Emitting write(a.foo)
          # Emitting a.foo
            # Emitting a
            movl -4(%ebp), %edi
          leal 4(%edi), %edi
          movl (%edi), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting a = new B()
          # Emitting a
          leal -4(%ebp), %edi
          # Emitting new B()
          sub $16, %esp
          movl $8, 0(%esp)
          call calloc
          add $16, %esp
          leal (CLASS_B), %ebx
          movl %ebx, (%eax)
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting a.foo = 2
          # Emitting a.foo
            # Emitting a
            movl -4(%ebp), %esi
          leal 4(%esi), %esi
          # Emitting 2
          movl $2, %edi
        movl %edi, (%esi)
        # Emitting write(a.foo)
          # Emitting a.foo
            # Emitting a
            movl -4(%ebp), %edi
          leal 4(%edi), %edi
          movl (%edi), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting b = new B()
          # Emitting b
          leal -8(%ebp), %edi
          # Emitting new B()
          sub $16, %esp
          movl $8, 0(%esp)
          call calloc
          add $16, %esp
          leal (CLASS_B), %ebx
          movl %ebx, (%eax)
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting b.foo = 3
          # Emitting b.foo
            # Emitting b
            movl -8(%ebp), %esi
