.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_A:
.int 0
.int A_foo
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $8, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class A {...}
    # Emitting int field
    # Emitting void foo(...) {...}
    A_foo:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(1)
          # Emitting 1
          movl $1, %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting write(this.field)
          # Emitting this.field
            # Emitting this
            movl 8(%ebp), %edi
          leal 4(%edi), %edi
          movl (%edi), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
  # Emitting class Main {...}
    # Emitting A[] x
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting this.x = new A[][2]
          # Emitting this.x
            # Emitting this
            movl 8(%ebp), %edi
          leal 4(%edi), %edi
          # Emitting new A[][2]
            # Emitting 2
            movl $2, %esi
          sub $16, %esp
          imul $4, %esi
          movl %esi, (%esp)
          call calloc
          add $16, %esp
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting i = 1
          # Emitting i
          leal -4(%ebp), %esi
          # Emitting 1
          movl $1, %edi
        movl %edi, (%esi)
        # Emitting write(i)
          # Emitting i
          movl -4(%ebp), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting this.x[i] = new A()
          # Emitting this.x[i]
            # Emitting i
<<<<<<< HEAD
            leal -4(%ebp), %edi
=======
            movl -4(%ebp), %edi
>>>>>>> 33e40de85d93a20932787addeeb2694641cdd611
            # Emitting this.x
              # Emitting this
              movl 8(%ebp), %esi
            leal 4(%esi), %esi
<<<<<<< HEAD
=======
            movl (%esi), %esi
>>>>>>> 33e40de85d93a20932787addeeb2694641cdd611
          leal (%esi, %edi, 4), %esi
          # Emitting new A()
          sub $16, %esp
          movl $8, 0(%esp)
          call calloc
          add $16, %esp
          leal (CLASS_A), %ebx
          movl %ebx, (%eax)
          movl %eax, %edi
        movl %edi, (%esi)
        # Emitting this.x[i].field = (i + 1)
          # Emitting this.x[i].field
            # Emitting this.x[i]
              # Emitting i
              movl -4(%ebp), %edi
              # Emitting this.x
                # Emitting this
                movl 8(%ebp), %esi
              leal 4(%esi), %esi
              movl (%esi), %esi
            movl (%esi, %edi, 4), %esi
          leal 4(%esi), %esi
          # Emitting (i + 1)
            # Emitting 1
            movl $1, %edi
            # Emitting i
            movl -4(%ebp), %edx
          add %edi, %edx
        movl %edx, (%esi)
        # Emitting i = this.x[1].field
          # Emitting i
          leal -4(%ebp), %edx
          # Emitting this.x[1].field
            # Emitting this.x[1]
              # Emitting 1
              movl $1, %esi
              # Emitting this.x
                # Emitting this
                movl 8(%ebp), %edi
              leal 4(%edi), %edi
              movl (%edi), %edi
            movl (%edi, %esi, 4), %edi
          leal 4(%edi), %edi
          movl (%edi), %edi
        movl %edi, (%edx)
        # Emitting write(i)
          # Emitting i
          movl -4(%ebp), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting this.x[1].foo(...)
          # Emitting this.x[1]
            # Emitting 1
            movl $1, %edi
            # Emitting this.x
              # Emitting this
              movl 8(%ebp), %edx
            leal 4(%edx), %edx
            movl (%edx), %edx
          movl (%edx, %edi, 4), %edx
        pushl %edx
        movl (%edx), %edx
        movl 4(%edx), %edx
        call *%edx
    pushl %edx
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
