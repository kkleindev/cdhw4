.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Record:
.int 0
.int Record_print
CLASS_Main:
.int 0
.int Main_swap
.int Main_sort
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $12, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Record {...}
    # Emitting int a
    # Emitting void print(...) {...}
    Record_print:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(this.a)
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %edi
          leal 4(%edi), %edi
          movl (%edi), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
  # Emitting class Main {...}
    # Emitting Record[] a
    # Emitting int i
    # Emitting void swap(...) {...}
    Main/swap/Record/Record:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting temp = r1.a
          # Emitting temp
          leal -4(%ebp), %edi
          # Emitting r1.a
            # Emitting r1
            movl 16(%ebp), %esi
          leal 4(%esi), %esi
          movl (%esi), %esi
        movl %esi, (%edi)
        # Emitting r1.a = r2.a
          # Emitting r1.a
            # Emitting r1
            movl 16(%ebp), %esi
          leal 4(%esi), %esi
          # Emitting r2.a
            # Emitting r2
            movl 12(%ebp), %edi
          leal 4(%edi), %edi
          movl (%edi), %edi
        movl %edi, (%esi)
        # Emitting r2.a = temp
          # Emitting r2.a
            # Emitting r2
            movl 12(%ebp), %edi
          leal 4(%edi), %edi
          # Emitting temp
          movl -4(%ebp), %esi
        movl %esi, (%edi)
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
    # Emitting void sort(...) {...}
    Main/sort/int/int:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting m = ((this.a[left].a + this.a[right].a) / 2)
          # Emitting m
          leal -12(%ebp), %esi
          # Emitting ((this.a[left].a + this.a[right].a) / 2)
            # Emitting (this.a[left].a + this.a[right].a)
              # Emitting this.a[right].a
                # Emitting this.a[right]
                  # Emitting right
                  movl 12(%ebp), %edi
                  # Emitting this.a
                    # Emitting this
                    movl 8(%ebp), %edx
                  leal 4(%edx), %edx
                  movl (%edx), %edx
                movl (%edx, %edi, 4), %edx
              leal 4(%edx), %edx
              movl (%edx), %edx
              # Emitting this.a[left].a
                # Emitting this.a[left]
                  # Emitting left
                  movl 16(%ebp), %edi
                  # Emitting this.a
                    # Emitting this
                    movl 8(%ebp), %ecx
                  leal 4(%ecx), %ecx
                  movl (%ecx), %ecx
                movl (%ecx, %edi, 4), %ecx
              leal 4(%ecx), %ecx
              movl (%ecx), %ecx
            add %edx, %ecx
            # Emitting 2
            movl $2, %edx
        movl %ecx, (%esi)
        # Emitting i = left
          # Emitting i
          leal -4(%ebp), %ecx
          # Emitting left
          movl 16(%ebp), %esi
        movl %esi, (%ecx)
        # Emitting j = right
          # Emitting j
          leal -8(%ebp), %esi
          # Emitting right
          movl 12(%ebp), %ecx
        movl %ecx, (%esi)
        # Emitting while ((i <= j)) {...}
          # Emitting (i <= j)
            # Emitting j
            movl -8(%ebp), %ecx
            # Emitting i
            movl -4(%ebp), %esi
          cmp %ecx, %esi
          jle label7
          movl $0, %esi
          jmp label6
          label7:
          movl $1, %esi
          label6:
        movl $1, %ecx
        cmpl %esi, %ecx
        jg label5
        label4:
          # Emitting (...)
            # Emitting while ((this.a[i].a < m)) {...}
              # Emitting (this.a[i].a < m)
                # Emitting this.a[i].a
                  # Emitting this.a[i]
                    # Emitting i
                    movl -4(%ebp), %esi
                    # Emitting this.a
                      # Emitting this
                      movl 8(%ebp), %edx
                    leal 4(%edx), %edx
                    movl (%edx), %edx
                  movl (%edx, %esi, 4), %edx
                leal 4(%edx), %edx
                movl (%edx), %edx
                # Emitting m
                movl -12(%ebp), %esi
              cmp %esi, %edx
              jl label11
              movl $0, %edx
              jmp label10
              label11:
              movl $1, %edx
              label10:
            movl $1, %esi
            cmpl %edx, %esi
            jg label9
            label8:
              # Emitting (...)
                # Emitting i = (i + 1)
                  # Emitting i
                  leal -4(%ebp), %edx
                  # Emitting (i + 1)
                    # Emitting 1
                    movl $1, %edi
                    # Emitting i
                    movl -4(%ebp), %ebx
                  add %edi, %ebx
                movl %ebx, (%edx)
              # Emitting (this.a[i].a < m)
                # Emitting this.a[i].a
                  # Emitting this.a[i]
                    # Emitting i
                    movl -4(%ebp), %ebx
                    # Emitting this.a
                      # Emitting this
                      movl 8(%ebp), %edx
                    leal 4(%edx), %edx
                    movl (%edx), %edx
                  movl (%edx, %ebx, 4), %edx
                leal 4(%edx), %edx
                movl (%edx), %edx
                # Emitting m
                movl -12(%ebp), %ebx
              cmp %ebx, %edx
              jl label15
              movl $0, %edx
              jmp label14
              label15:
              movl $1, %edx
              label14:
            cmp %edx, %esi
            jg label9
            jmp label8
            label9:
            # Emitting while ((this.a[j].a > m)) {...}
              # Emitting (this.a[j].a > m)
                # Emitting this.a[j].a
                  # Emitting this.a[j]
                    # Emitting j
                    movl -8(%ebp), %esi
                    # Emitting this.a
                      # Emitting this
                      movl 8(%ebp), %ebx
                    leal 4(%ebx), %ebx
                    movl (%ebx), %ebx
                  movl (%ebx, %esi, 4), %ebx
                leal 4(%ebx), %ebx
                movl (%ebx), %ebx
                # Emitting m
                movl -12(%ebp), %esi
              cmp %esi, %ebx
              jg label19
              movl $0, %ebx
              jmp label18
              label19:
              movl $1, %ebx
              label18:
            movl $1, %esi
            cmpl %ebx, %esi
            jg label17
            label16:
              # Emitting (...)
                # Emitting j = (j - 1)
                  # Emitting j
                  leal -8(%ebp), %ebx
                  # Emitting (j - 1)
                    # Emitting 1
                    movl $1, %edi
                    # Emitting j
                    movl -8(%ebp), %eax
                  sub %edi, %eax
                movl %eax, (%ebx)
              # Emitting (this.a[j].a > m)
                # Emitting this.a[j].a
                  # Emitting this.a[j]
                    # Emitting j
                    movl -8(%ebp), %eax
                    # Emitting this.a
                      # Emitting this
                      movl 8(%ebp), %ebx
                    leal 4(%ebx), %ebx
                    movl (%ebx), %ebx
                  movl (%ebx, %eax, 4), %ebx
                leal 4(%ebx), %ebx
                movl (%ebx), %ebx
                # Emitting m
                movl -12(%ebp), %eax
              cmp %eax, %ebx
              jg label23
              movl $0, %ebx
              jmp label22
              label23:
              movl $1, %ebx
              label22:
            cmp %ebx, %esi
            jg label17
            jmp label16
            label17:
            # Emitting if ((i <= j)) {...} else {...}
              # Emitting (i <= j)
                # Emitting j
                movl -8(%ebp), %esi
                # Emitting i
                movl -4(%ebp), %eax
              cmp %esi, %eax
              jle label27
              movl $0, %eax
              jmp label26
              label27:
              movl $1, %eax
              label26:
            movl $1, %esi
            cmpl %eax, %esi
            jg label25
              # Emitting (...)
                # Emitting this.swap(...)
                pushl %eax
                pushl %ebx
                pushl %ecx
                pushl %edx
                  # Emitting this.a[i]
                    # Emitting i
                    movl -4(%ebp), %edx
                    # Emitting this.a
                      # Emitting this
                      movl 8(%ebp), %ecx
                    leal 4(%ecx), %ecx
                    movl (%ecx), %ecx
                  movl (%ecx, %edx, 4), %ecx
                sub $16, %esp
                movl $8, 0(%esp)
                call calloc
                add $16, %esp
                leal (CLASS_Record), %ebx
                movl %ebx, (%eax)
                movl 4(%ecx), %ebx
                movl %ebx, 4(%eax)
                pushl %eax
                  # Emitting this.a[j]
                    # Emitting j
                    movl -8(%ebp), %ecx
                    # Emitting this.a
                      # Emitting this
                      movl 8(%ebp), %edx
                    leal 4(%edx), %edx
                    movl (%edx), %edx
                  movl (%edx, %ecx, 4), %edx
                sub $16, %esp
                movl $8, 0(%esp)
                call calloc
                add $16, %esp
                leal (CLASS_Record), %ebx
                movl %ebx, (%eax)
                movl 4(%edx), %ebx
                movl %ebx, 4(%eax)
                pushl %eax
                  # Emitting this
                  movl 8(%ebp), %edx
                pushl %edx
                movl (%edx), %edx
                movl 4(%edx), %edx
                call *%edx
                popl %ebx
                popl %ebx
                # Emitting i = (i + 1)
                  # Emitting i
                  leal -4(%ebp), %ecx
                  # Emitting (i + 1)
                    # Emitting 1
                    movl $1, %ebx
                    # Emitting i
                    movl -4(%ebp), %eax
                  add %ebx, %eax
                movl %eax, (%ecx)
                # Emitting j = (j - 1)
                  # Emitting j
                  leal -8(%ebp), %eax
                  # Emitting (j - 1)
                    # Emitting 1
                    movl $1, %ecx
                    # Emitting j
                    movl -8(%ebp), %ebx
                  sub %ecx, %ebx
                movl %ebx, (%eax)
            jmp label24
            label25:
              # Emitting nop
            label24:
          # Emitting (i <= j)
            # Emitting j
            movl -8(%ebp), %ebx
            # Emitting i
            movl -4(%ebp), %eax
          cmp %ebx, %eax
          jle label33
          movl $0, %eax
          jmp label32
          label33:
          movl $1, %eax
          label32:
        cmp %eax, %ecx
        jg label5
        jmp label4
        label5:
        # Emitting if ((left < j)) {...} else {...}
          # Emitting (left < j)
            # Emitting j
            movl -8(%ebp), %ebx
            # Emitting left
            movl 16(%ebp), %ecx
          cmp %ebx, %ecx
          jl label37
          movl $0, %ecx
          jmp label36
          label37:
          movl $1, %ecx
          label36:
        movl $1, %ebx
        cmpl %ecx, %ebx
        jg label35
          # Emitting (...)
            # Emitting this.sort(...)
            pushl %eax
            pushl %ecx
            pushl %edx
              # Emitting left
              movl 16(%ebp), %edx
            pushl %edx
              # Emitting j
              movl -8(%ebp), %edx
            pushl %edx
              # Emitting this
              movl 8(%ebp), %edx
            pushl %edx
            movl (%edx), %edx
            movl 8(%edx), %edx
            call *%edx
            popl %ebx
            popl %ebx
        jmp label34
        label35:
          # Emitting nop
        label34:
        # Emitting if ((i < right)) {...} else {...}
          # Emitting (i < right)
            # Emitting right
            movl 12(%ebp), %ecx
            # Emitting i
            movl -4(%ebp), %eax
          cmp %ecx, %eax
          jl label41
          movl $0, %eax
          jmp label40
          label41:
          movl $1, %eax
          label40:
        movl $1, %ecx
        cmpl %eax, %ecx
        jg label39
          # Emitting (...)
            # Emitting this.sort(...)
            pushl %eax
            pushl %edx
              # Emitting i
              movl -4(%ebp), %edx
            pushl %edx
              # Emitting right
              movl 12(%ebp), %edx
            pushl %edx
              # Emitting this
              movl 8(%ebp), %edx
            pushl %edx
            movl (%edx), %edx
            movl 8(%edx), %edx
            call *%edx
            popl %ebx
            popl %ebx
        jmp label38
        label39:
          # Emitting nop
        label38:
    pushl %edx
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting SIZE = 5
          # Emitting SIZE
          leal -4(%ebp), %edx
          # Emitting 5
          movl $5, %eax
        movl %eax, (%edx)
        # Emitting this.a = new Record[][SIZE]
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %eax
          leal 4(%eax), %eax
          # Emitting new Record[][SIZE]
            # Emitting SIZE
            movl -4(%ebp), %edx
          pushl %eax
          sub $16, %esp
          imul $4, %edx
          movl %edx, (%esp)
          call calloc
          add $16, %esp
          movl %eax, %edx
        movl %edx, (%eax)
        # Emitting j = 0
          # Emitting j
          leal -8(%ebp), %edx
          # Emitting 0
          movl $0, %eax
        movl %eax, (%edx)
        # Emitting while ((j < SIZE)) {...}
          # Emitting (j < SIZE)
            # Emitting SIZE
            movl -4(%ebp), %eax
            # Emitting j
            movl -8(%ebp), %edx
          cmp %eax, %edx
          jl label45
          movl $0, %edx
          jmp label44
          label45:
          movl $1, %edx
          label44:
        movl $1, %eax
        cmpl %edx, %eax
        jg label43
        label42:
          # Emitting (...)
            # Emitting this.a[j] = new Record()
              # Emitting this.a[j]
                # Emitting j
<<<<<<< HEAD
                leal -8(%ebp), %edx
=======
                movl -8(%ebp), %edx
>>>>>>> 33e40de85d93a20932787addeeb2694641cdd611
                # Emitting this.a
                  # Emitting this
                  movl 8(%ebp), %ecx
                leal 4(%ecx), %ecx
<<<<<<< HEAD
=======
                movl (%ecx), %ecx
>>>>>>> 33e40de85d93a20932787addeeb2694641cdd611
              leal (%ecx, %edx, 4), %ecx
              # Emitting new Record()
              pushl %eax
              sub $16, %esp
              movl $8, 0(%esp)
              call calloc
              add $16, %esp
              leal (CLASS_Record), %ebx
              movl %ebx, (%eax)
            movl %eax, (%ecx)
            # Emitting j = (j + 1)
              # Emitting j
              leal -8(%ebp), %eax
              # Emitting (j + 1)
                # Emitting 1
                movl $1, %ecx
                # Emitting j
                movl -8(%ebp), %edx
              add %ecx, %edx
            movl %edx, (%eax)
          # Emitting (j < SIZE)
            # Emitting SIZE
            movl -4(%ebp), %edx
            # Emitting j
            movl -8(%ebp), %eax
          cmp %edx, %eax
          jl label49
          movl $0, %eax
          jmp label48
          label49:
          movl $1, %eax
          label48:
        cmp %eax, %eax
        jg label43
        jmp label42
        label43:
        # Emitting this.a[0].a = 5
          # Emitting this.a[0].a
            # Emitting this.a[0]
              # Emitting 0
              movl $0, %edx
              # Emitting this.a
                # Emitting this
                movl 8(%ebp), %ecx
              leal 4(%ecx), %ecx
              movl (%ecx), %ecx
            movl (%ecx, %edx, 4), %ecx
          leal 4(%ecx), %ecx
          # Emitting 5
          movl $5, %edx
        movl %edx, (%ecx)
        # Emitting this.a[1].a = 3
          # Emitting this.a[1].a
            # Emitting this.a[1]
              # Emitting 1
              movl $1, %edx
              # Emitting this.a
                # Emitting this
                movl 8(%ebp), %ecx
              leal 4(%ecx), %ecx
              movl (%ecx), %ecx
            movl (%ecx, %edx, 4), %ecx
          leal 4(%ecx), %ecx
          # Emitting 3
          movl $3, %edx
        movl %edx, (%ecx)
        # Emitting this.a[2].a = 1
          # Emitting this.a[2].a
            # Emitting this.a[2]
              # Emitting 2
              movl $2, %edx
              # Emitting this.a
                # Emitting this
                movl 8(%ebp), %ecx
              leal 4(%ecx), %ecx
              movl (%ecx), %ecx
            movl (%ecx, %edx, 4), %ecx
          leal 4(%ecx), %ecx
          # Emitting 1
          movl $1, %edx
        movl %edx, (%ecx)
        # Emitting this.a[3].a = 4
          # Emitting this.a[3].a
            # Emitting this.a[3]
              # Emitting 3
              movl $3, %edx
              # Emitting this.a
                # Emitting this
                movl 8(%ebp), %ecx
              leal 4(%ecx), %ecx
              movl (%ecx), %ecx
            movl (%ecx, %edx, 4), %ecx
          leal 4(%ecx), %ecx
          # Emitting 4
          movl $4, %edx
        movl %edx, (%ecx)
        # Emitting this.a[4].a = 2
          # Emitting this.a[4].a
            # Emitting this.a[4]
              # Emitting 4
              movl $4, %edx
              # Emitting this.a
                # Emitting this
                movl 8(%ebp), %ecx
              leal 4(%ecx), %ecx
              movl (%ecx), %ecx
            movl (%ecx, %edx, 4), %ecx
          leal 4(%ecx), %ecx
          # Emitting 2
          movl $2, %edx
        movl %edx, (%ecx)
        # Emitting j = 0
          # Emitting j
          leal -8(%ebp), %edx
          # Emitting 0
          movl $0, %ecx
        movl %ecx, (%edx)
        # Emitting while ((j < SIZE)) {...}
          # Emitting (j < SIZE)
            # Emitting SIZE
            movl -4(%ebp), %ecx
            # Emitting j
            movl -8(%ebp), %edx
          cmp %ecx, %edx
          jl label53
          movl $0, %edx
          jmp label52
          label53:
          movl $1, %edx
          label52:
        movl $1, %ecx
        cmpl %edx, %ecx
        jg label51
        label50:
          # Emitting (...)
            # Emitting this.a[j].print(...)
            pushl %eax
            pushl %ecx
              # Emitting this.a[j]
                # Emitting j
                movl -8(%ebp), %ecx
                # Emitting this.a
                  # Emitting this
                  movl 8(%ebp), %eax
                leal 4(%eax), %eax
                movl (%eax), %eax
              movl (%eax, %ecx, 4), %eax
            pushl %eax
            movl (%eax), %eax
            movl 4(%eax), %eax
            call *%eax
            # Emitting j = (j + 1)
              # Emitting j
              leal -8(%ebp), %ecx
              # Emitting (j + 1)
                # Emitting 1
                movl $1, %edx
                # Emitting j
                movl -8(%ebp), %ebx
              add %edx, %ebx
            movl %ebx, (%ecx)
          # Emitting (j < SIZE)
            # Emitting SIZE
            movl -4(%ebp), %ebx
            # Emitting j
            movl -8(%ebp), %ecx
          cmp %ebx, %ecx
          jl label57
          movl $0, %ecx
          jmp label56
          label57:
          movl $1, %ecx
          label56:
        cmp %ecx, %ecx
        jg label51
        jmp label50
        label51:
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting this.sort(...)
        pushl %eax
        pushl %ecx
          # Emitting 0
          movl $0, %ecx
        pushl %ecx
          # Emitting 4
          movl $4, %ecx
        pushl %ecx
          # Emitting this
          movl 8(%ebp), %ecx
        pushl %ecx
        movl (%ecx), %ecx
        movl 8(%ecx), %ecx
        call *%ecx
        popl %ebx
        popl %ebx
        # Emitting j = 0
          # Emitting j
          leal -8(%ebp), %eax
          # Emitting 0
          movl $0, %ebx
        movl %ebx, (%eax)
        # Emitting while ((j < SIZE)) {...}
          # Emitting (j < SIZE)
            # Emitting SIZE
            movl -4(%ebp), %ebx
            # Emitting j
            movl -8(%ebp), %eax
          cmp %ebx, %eax
          jl label61
          movl $0, %eax
          jmp label60
          label61:
          movl $1, %eax
          label60:
        movl $1, %ebx
        cmpl %eax, %ebx
        jg label59
        label58:
          # Emitting (...)
            # Emitting this.a[j].print(...)
            pushl %ebx
            pushl %ecx
              # Emitting this.a[j]
                # Emitting j
                movl -8(%ebp), %ecx
                # Emitting this.a
                  # Emitting this
                  movl 8(%ebp), %ebx
                leal 4(%ebx), %ebx
                movl (%ebx), %ebx
              movl (%ebx, %ecx, 4), %ebx
            pushl %ebx
            movl (%ebx), %ebx
            movl 4(%ebx), %ebx
            call *%ebx
            # Emitting j = (j + 1)
              # Emitting j
              leal -8(%ebp), %ecx
              # Emitting (j + 1)
                # Emitting 1
                movl $1, %eax
                # Emitting j
                movl -8(%ebp), %edx
              add %eax, %edx
            movl %edx, (%ecx)
          # Emitting (j < SIZE)
            # Emitting SIZE
            movl -4(%ebp), %edx
            # Emitting j
            movl -8(%ebp), %ecx
          cmp %edx, %ecx
          jl label65
          movl $0, %ecx
          jmp label64
          label65:
          movl $1, %ecx
          label64:
        cmp %ecx, %ebx
        jg label59
        jmp label58
        label59:
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    pushl %ebx
    pushl %ecx
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
