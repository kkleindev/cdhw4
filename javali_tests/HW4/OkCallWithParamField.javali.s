.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_A:
.int 0
.int A_foo
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class A {...}
    # Emitting int i
    # Emitting void foo(...) {...}
    A_foo:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(p)
          # Emitting p
          movl 12(%ebp), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting write(this.i)
          # Emitting this.i
            # Emitting this
            movl 8(%ebp), %edi
          leal 4(%edi), %edi
          movl (%edi), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting a = new A()
          # Emitting a
          leal -4(%ebp), %edi
          # Emitting new A()
          sub $16, %esp
          movl $8, 0(%esp)
          call calloc
          add $16, %esp
          leal (CLASS_A), %ebx
          movl %ebx, (%eax)
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting a.i = 10
          # Emitting a.i
            # Emitting a
            movl -4(%ebp), %esi
          leal 4(%esi), %esi
          # Emitting 10
          movl $10, %edi
        movl %edi, (%esi)
        # Emitting a.foo(...)
          # Emitting 1
          movl $1, %edi
        pushl %edi
          # Emitting a
          movl -4(%ebp), %edi
        pushl %edi
        movl (%edi), %edi
        movl 4(%edi), %edi
        call *%edi
        popl %ebx
        # Emitting a.foo(...)
        pushl %edi
          # Emitting 2
          movl $2, %edi
        pushl %edi
          # Emitting a
          movl -4(%ebp), %edi
        pushl %edi
        movl (%edi), %edi
        movl 4(%edi), %edi
        call *%edi
        popl %ebx
        # Emitting a.foo(...)
        pushl %edi
          # Emitting 3
          movl $3, %edi
        pushl %edi
          # Emitting a
          movl -4(%ebp), %edi
        pushl %edi
        movl (%edi), %edi
        movl 4(%edi), %edi
        call *%edi
        popl %ebx
    pushl %edi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
