.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.int Main_fib
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting a = this.fib(...)
          # Emitting a
          leal -4(%ebp), %edi
          # Emitting this.fib(...)
          pushl %edi
            # Emitting 20
            movl $20, %edi
          pushl %edi
            # Emitting this
            movl 8(%ebp), %edi
          pushl %edi
          movl (%edi), %edi
          movl 8(%edi), %edi
          call *%edi
          popl %ebx
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting write(a)
          # Emitting a
          movl -4(%ebp), %esi
        sub $16, %esp
        movl %esi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    pushl %edi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
    # Emitting int fib(...) {...}
    Main_fib:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting if ((n <= 1)) {...} else {...}
          # Emitting (n <= 1)
            # Emitting 1
            movl $1, %edi
            # Emitting n
            movl 12(%ebp), %esi
          cmp %edi, %esi
          jle label3
          movl $0, %esi
          jmp label2
          label3:
          movl $1, %esi
          label2:
        movl $1, %edi
        cmpl %esi, %edi
        jg label1
          # Emitting (...)
            # Emitting fib = n
              # Emitting fib
              leal -4(%ebp), %edi
              # Emitting n
              movl 12(%ebp), %edx
            movl %edx, (%edi)
        jmp label0
        label1:
          # Emitting (...)
            # Emitting fib = this.fib(...)
              # Emitting fib
              leal -4(%ebp), %edx
              # Emitting this.fib(...)
              pushl %edx
              pushl %esi
                # Emitting (n - 1)
                  # Emitting 1
                  movl $1, %esi
                  # Emitting n
                  movl 12(%ebp), %edx
                sub %esi, %edx
              pushl %edx
                # Emitting this
                movl 8(%ebp), %edx
              pushl %edx
              movl (%edx), %edx
              movl 8(%edx), %edx
              call *%edx
              popl %ebx
              movl %eax, %esi
            movl %esi, (%edx)
            # Emitting fib2 = this.fib(...)
              # Emitting fib2
              leal -8(%ebp), %esi
              # Emitting this.fib(...)
              pushl %edx
              pushl %esi
                # Emitting (n - 2)
                  # Emitting 2
                  movl $2, %esi
                  # Emitting n
                  movl 12(%ebp), %edx
                sub %esi, %edx
              pushl %edx
                # Emitting this
                movl 8(%ebp), %edx
              pushl %edx
              movl (%edx), %edx
              movl 8(%edx), %edx
              call *%edx
              popl %ebx
              movl %eax, %esi
            movl %esi, (%esi)
            # Emitting fib = (fib + fib2)
              # Emitting fib
              leal -4(%ebp), %esi
              # Emitting (fib + fib2)
                # Emitting fib2
                movl -8(%ebp), %edi
                # Emitting fib
                movl -4(%ebp), %ecx
              add %edi, %ecx
            movl %ecx, (%esi)
        label0:
        # Emitting return fib
        pushl %edx
          # Emitting fib
          movl -4(%ebp), %edx
        movl %edx, %eax
        pushl %edx
        leal 4(%ebp), %esp
        movl (%ebp), %ebp
        ret
