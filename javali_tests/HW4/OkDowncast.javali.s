.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_A:
.int 0
CLASS_B:
.int CLASS_A
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class A {...}
  # Emitting class B {...}
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting a = new B()
          # Emitting a
          leal -4(%ebp), %edi
          # Emitting new B()
          sub $16, %esp
          movl $4, 0(%esp)
          call calloc
          add $16, %esp
          leal (CLASS_B), %ebx
          movl %ebx, (%eax)
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting b = (B)(a)
          # Emitting b
          leal -8(%ebp), %esi
          # Emitting (B)(a)
