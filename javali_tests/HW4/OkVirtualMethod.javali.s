.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_A:
.int 0
.int A_override
.int A_base
CLASS_B:
.int CLASS_A
.int A_base
.int B_override
.int B_sub
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class A {...}
    # Emitting void override(...) {...}
    A_override:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(0)
          # Emitting 0
          movl $0, %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
    # Emitting void base(...) {...}
    A_base:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(1)
          # Emitting 1
          movl $1, %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
  # Emitting class B {...}
    # Emitting void override(...) {...}
    B_override:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(2)
          # Emitting 2
          movl $2, %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
    # Emitting void sub(...) {...}
    B_sub:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(3)
          # Emitting 3
          movl $3, %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting a = null
          # Emitting a
          leal -4(%ebp), %edi
          # Emitting null
          movl $0, %esi
        movl %esi, (%edi)
        # Emitting b = null
          # Emitting b
          leal -8(%ebp), %esi
          # Emitting null
          movl $0, %edi
        movl %edi, (%esi)
        # Emitting a = new A()
          # Emitting a
          leal -4(%ebp), %edi
          # Emitting new A()
          sub $16, %esp
          movl $4, 0(%esp)
          call calloc
          add $16, %esp
          leal (CLASS_A), %ebx
          movl %ebx, (%eax)
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting a.base(...)
          # Emitting a
          movl -4(%ebp), %esi
        pushl %esi
        movl (%esi), %esi
        movl 8(%esi), %esi
        call *%esi
        # Emitting a.override(...)
        pushl %esi
          # Emitting a
          movl -4(%ebp), %esi
        pushl %esi
        movl (%esi), %esi
        movl 4(%esi), %esi
        call *%esi
        # Emitting b = new B()
          # Emitting b
          leal -8(%ebp), %edi
          # Emitting new B()
          sub $16, %esp
          movl $4, 0(%esp)
          call calloc
          add $16, %esp
          leal (CLASS_B), %ebx
          movl %ebx, (%eax)
          movl %eax, %edx
        movl %edx, (%edi)
        # Emitting b.base(...)
        pushl %esi
          # Emitting b
          movl -8(%ebp), %esi
        pushl %esi
        movl (%esi), %esi
        movl 8(%esi), %esi
        call *%esi
        # Emitting b.override(...)
        pushl %esi
          # Emitting b
          movl -8(%ebp), %esi
        pushl %esi
        movl (%esi), %esi
        movl 0(%esi), %esi
        call *%esi
        # Emitting b.sub(...)
        pushl %esi
          # Emitting b
          movl -8(%ebp), %esi
        pushl %esi
        movl (%esi), %esi
        movl 4(%esi), %esi
        call *%esi
        # Emitting a = b
          # Emitting a
          leal -4(%ebp), %edx
          # Emitting b
          movl -8(%ebp), %edi
        movl %edi, (%edx)
        # Emitting a.base(...)
        pushl %esi
          # Emitting a
          movl -4(%ebp), %esi
        pushl %esi
        movl (%esi), %esi
        movl 8(%esi), %esi
        call *%esi
        # Emitting a.override(...)
        pushl %esi
          # Emitting a
          movl -4(%ebp), %esi
        pushl %esi
        movl (%esi), %esi
        movl 4(%esi), %esi
        call *%esi
        # Emitting b.base(...)
        pushl %esi
          # Emitting b
          movl -8(%ebp), %esi
        pushl %esi
        movl (%esi), %esi
        movl 8(%esi), %esi
        call *%esi
        # Emitting b.override(...)
        pushl %esi
          # Emitting b
          movl -8(%ebp), %esi
        pushl %esi
        movl (%esi), %esi
        movl 0(%esi), %esi
        call *%esi
        # Emitting b.sub(...)
        pushl %esi
          # Emitting b
          movl -8(%ebp), %esi
        pushl %esi
        movl (%esi), %esi
        movl 4(%esi), %esi
        call *%esi
    pushl %esi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
