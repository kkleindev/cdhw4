.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_m
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting int m(...) {...}
    Main_m:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting return 0
          # Emitting 0
          movl $0, %edi
        movl %edi, %eax
        pushl %edi
        leal 4(%ebp), %esp
        movl (%ebp), %ebp
        ret
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting res = -(1)
          # Emitting res
          leal -4(%ebp), %edi
          # Emitting -(1)
            # Emitting 1
            movl $1, %esi
          negl %esi
        movl %esi, (%edi)
        # Emitting res = this.m(...)
          # Emitting res
          leal -4(%ebp), %esi
          # Emitting this.m(...)
          pushl %esi
            # Emitting this
            movl 8(%ebp), %esi
          pushl %esi
          movl (%esi), %esi
          movl 4(%esi), %esi
          call *%esi
          movl %eax, %edi
        movl %edi, (%esi)
        # Emitting write(res)
          # Emitting res
          movl -4(%ebp), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting res = this.m(...)
          # Emitting res
          leal -4(%ebp), %edi
          # Emitting this.m(...)
          pushl %esi
          pushl %edi
            # Emitting this
            movl 8(%ebp), %edi
          pushl %edi
          movl (%edi), %edi
          movl 4(%edi), %edi
          call *%edi
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting write(res)
          # Emitting res
          movl -4(%ebp), %esi
        sub $16, %esp
        movl %esi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
    pushl %edi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
