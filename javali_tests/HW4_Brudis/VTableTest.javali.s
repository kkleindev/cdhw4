.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
CLASS_A:
.int CLASS_B
.int B_method1
CLASS_B:
.int CLASS_C
.int B_method1
CLASS_C:
.int 0
.int C_method1
.section .text
.globl main
main:
sub $16, %esp
movl $16, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting int field1
    # Emitting int field2
    # Emitting int field3
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(5)
          # Emitting 5
          movl $5, %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
  # Emitting class A {...}
    # Emitting int field1
  # Emitting class B {...}
    # Emitting int field2
    # Emitting void method1(...) {...}
    B_method1:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(7)
          # Emitting 7
          movl $7, %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
  # Emitting class C {...}
    # Emitting int field3
    # Emitting void method1(...) {...}
    C_method1:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(6)
          # Emitting 6
          movl $6, %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
