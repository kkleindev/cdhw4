.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_m
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting int m(...) {...}
    Main_m:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting return 1
          # Emitting 1
          movl $1, %edi
        movl %edi, %eax
        pushl %edi
        leal 4(%ebp), %esp
        movl (%ebp), %ebp
        ret
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting res = -(1)
          # Emitting res
          leal -4(%ebp), %edi
          # Emitting -(1)
            # Emitting 1
            movl $1, %esi
          negl %esi
        movl %esi, (%edi)
        # Emitting res = this.m(...)
          # Emitting res
          leal -4(%ebp), %esi
          # Emitting this.m(...)
          pushl %esi
            # Emitting 1
            movl $1, %esi
          pushl %esi
            # Emitting this
            movl 8(%ebp), %esi
          pushl %esi
          movl (%esi), %esi
          movl 4(%esi), %esi
          call *%esi
          popl %ebx
          movl %eax, %edi
        movl %edi, (%esi)
    pushl %esi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
