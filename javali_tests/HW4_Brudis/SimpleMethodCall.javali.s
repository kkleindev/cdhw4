.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.int Main_meth1
.int Main_meth2
.int Main_meth3
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting this.meth1(...)
          # Emitting this
          movl 8(%ebp), %edi
        pushl %edi
        movl (%edi), %edi
        movl 8(%edi), %edi
        call *%edi
        # Emitting this.meth2(...)
        pushl %edi
          # Emitting 5
          movl $5, %edi
        pushl %edi
          # Emitting this
          movl 8(%ebp), %edi
        pushl %edi
        movl (%edi), %edi
        movl 12(%edi), %edi
        call *%edi
        popl %ebx
        # Emitting i = this.meth3(...)
          # Emitting i
          leal -4(%ebp), %esi
          # Emitting this.meth3(...)
          pushl %esi
          pushl %edi
            # Emitting 5
            movl $5, %edi
          pushl %edi
            # Emitting 5
            movl $5, %edi
          pushl %edi
            # Emitting this
            movl 8(%ebp), %edi
          pushl %edi
          movl (%edi), %edi
          movl 16(%edi), %edi
          call *%edi
          popl %ebx
          popl %ebx
          movl %eax, %esi
        movl %esi, (%esi)
    pushl %edi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
    # Emitting void meth1(...) {...}
    Main_meth1:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting write(5)
          # Emitting 5
          movl $5, %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
    # Emitting void meth2(...) {...}
    Main_meth2:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting while ((i > 0)) {...}
          # Emitting (i > 0)
            # Emitting 0
            movl $0, %edi
            # Emitting i
            movl 12(%ebp), %esi
          cmp %edi, %esi
          jg label3
          movl $0, %esi
          jmp label2
          label3:
          movl $1, %esi
          label2:
        movl $1, %edi
        cmpl %esi, %edi
        jg label1
        label0:
          # Emitting (...)
            # Emitting write(5)
              # Emitting 5
              movl $5, %esi
            sub $16, %esp
            movl %esi, 4(%esp)
            movl $STR_D, 0(%esp)
            call printf
            add $16, %esp
            # Emitting i = (i - 1)
              # Emitting i
              leal 12(%ebp), %esi
              # Emitting (i - 1)
                # Emitting 1
                movl $1, %edx
                # Emitting i
                movl 12(%ebp), %ecx
              sub %edx, %ecx
            movl %ecx, (%esi)
          # Emitting (i > 0)
            # Emitting 0
            movl $0, %ecx
            # Emitting i
            movl 12(%ebp), %esi
          cmp %ecx, %esi
          jg label7
          movl $0, %esi
          jmp label6
          label7:
          movl $1, %esi
          label6:
        cmp %esi, %edi
        jg label1
        jmp label0
        label1:
    pushl %esi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
    # Emitting int meth3(...) {...}
    Main_meth3:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting while ((i1 > 0)) {...}
          # Emitting (i1 > 0)
            # Emitting 0
            movl $0, %esi
            # Emitting i1
            movl 16(%ebp), %edi
          cmp %esi, %edi
          jg label11
          movl $0, %edi
          jmp label10
          label11:
          movl $1, %edi
          label10:
        movl $1, %esi
        cmpl %edi, %esi
        jg label9
        label8:
          # Emitting (...)
            # Emitting write(i2)
              # Emitting i2
              movl 12(%ebp), %edi
            sub $16, %esp
            movl %edi, 4(%esp)
            movl $STR_D, 0(%esp)
            call printf
            add $16, %esp
            # Emitting i1 = (i1 - 1)
              # Emitting i1
              leal 16(%ebp), %edi
              # Emitting (i1 - 1)
                # Emitting 1
                movl $1, %ecx
                # Emitting i1
                movl 16(%ebp), %edx
              sub %ecx, %edx
            movl %edx, (%edi)
          # Emitting (i1 > 0)
            # Emitting 0
            movl $0, %edx
            # Emitting i1
            movl 16(%ebp), %edi
          cmp %edx, %edi
          jg label15
          movl $0, %edi
          jmp label14
          label15:
          movl $1, %edi
          label14:
        cmp %edi, %esi
        jg label9
        jmp label8
        label9:
        # Emitting return 5
        pushl %edi
          # Emitting 5
          movl $5, %edi
        movl %edi, %eax
        pushl %edi
        leal 4(%ebp), %esp
        movl (%ebp), %ebp
        ret
