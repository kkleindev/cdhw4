.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting i = 1
          # Emitting i
          leal -4(%ebp), %edi
          # Emitting 1
          movl $1, %esi
        movl %esi, (%edi)
        # Emitting if (((i == 1) && true)) {...} else {...}
          # Emitting ((i == 1) && true)
            # Emitting (i == 1)
              # Emitting 1
              movl $1, %esi
              # Emitting i
              movl -4(%ebp), %edi
            cmp %esi, %edi
            je label5
            movl $0, %edi
            jmp label4
            label5:
            movl $1, %edi
            label4:
            # Emitting true
            movl $1, %esi
          and %esi, %edi
        movl $1, %esi
        cmpl %edi, %esi
        jg label1
          # Emitting (...)
            # Emitting write(i)
              # Emitting i
              movl -4(%ebp), %esi
            sub $16, %esp
            movl %esi, 4(%esp)
            movl $STR_D, 0(%esp)
            call printf
            add $16, %esp
        jmp label0
        label1:
          # Emitting (...)
            # Emitting write(0)
              # Emitting 0
              movl $0, %esi
            sub $16, %esp
            movl %esi, 4(%esp)
            movl $STR_D, 0(%esp)
            call printf
            add $16, %esp
        label0:
    pushl %edi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
