.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
    pushl $0
      # Emitting (...)
        # Emitting i0 = 0
          # Emitting i0
          leal -16(%ebp), %edi
          # Emitting 0
          movl $0, %esi
        movl %esi, (%edi)
        # Emitting i1 = 1
          # Emitting i1
          leal -20(%ebp), %esi
          # Emitting 1
          movl $1, %edi
        movl %edi, (%esi)
        # Emitting i2 = 2
          # Emitting i2
          leal -24(%ebp), %edi
          # Emitting 2
          movl $2, %esi
        movl %esi, (%edi)
        # Emitting i3 = 3
          # Emitting i3
          leal -28(%ebp), %esi
          # Emitting 3
          movl $3, %edi
        movl %edi, (%esi)
        # Emitting i4 = 4
          # Emitting i4
          leal -32(%ebp), %edi
          # Emitting 4
          movl $4, %esi
        movl %esi, (%edi)
        # Emitting i5 = 5
          # Emitting i5
          leal -36(%ebp), %esi
          # Emitting 5
          movl $5, %edi
        movl %edi, (%esi)
        # Emitting i6 = 6
          # Emitting i6
          leal -40(%ebp), %edi
          # Emitting 6
          movl $6, %esi
        movl %esi, (%edi)
        # Emitting i7 = 7
          # Emitting i7
          leal -44(%ebp), %esi
          # Emitting 7
          movl $7, %edi
        movl %edi, (%esi)
        # Emitting i8 = 3
          # Emitting i8
          leal -48(%ebp), %edi
          # Emitting 3
          movl $3, %esi
        movl %esi, (%edi)
        # Emitting i9 = 4
          # Emitting i9
          leal -52(%ebp), %esi
          # Emitting 4
          movl $4, %edi
        movl %edi, (%esi)
        # Emitting i10 = 5
          # Emitting i10
          leal -56(%ebp), %edi
          # Emitting 5
          movl $5, %esi
        movl %esi, (%edi)
        # Emitting i11 = 6
          # Emitting i11
          leal -60(%ebp), %esi
          # Emitting 6
          movl $6, %edi
        movl %edi, (%esi)
        # Emitting i12 = 7
          # Emitting i12
          leal -64(%ebp), %edi
          # Emitting 7
          movl $7, %esi
        movl %esi, (%edi)
        # Emitting r1 = (i0 + (i1 + (i2 + (i3 + (i4 + (i5 + (i6 + i7)))))))
          # Emitting r1
          leal -4(%ebp), %esi
          # Emitting (i0 + (i1 + (i2 + (i3 + (i4 + (i5 + (i6 + i7)))))))
            # Emitting (i1 + (i2 + (i3 + (i4 + (i5 + (i6 + i7))))))
              # Emitting (i2 + (i3 + (i4 + (i5 + (i6 + i7)))))
                # Emitting (i3 + (i4 + (i5 + (i6 + i7))))
                  # Emitting (i4 + (i5 + (i6 + i7)))
                    # Emitting (i5 + (i6 + i7))
                      # Emitting (i6 + i7)
                        # Emitting i7
                        movl -44(%ebp), %edi
                        # Emitting i6
                        movl -40(%ebp), %edx
                      add %edi, %edx
                      # Emitting i5
                      movl -36(%ebp), %edi
                    add %edx, %edi
                    # Emitting i4
                    movl -32(%ebp), %edx
                  add %edi, %edx
                  # Emitting i3
                  movl -28(%ebp), %edi
                add %edx, %edi
                # Emitting i2
                movl -24(%ebp), %edx
              add %edi, %edx
              # Emitting i1
              movl -20(%ebp), %edi
            add %edx, %edi
            # Emitting i0
            movl -16(%ebp), %edx
          add %edi, %edx
        movl %edx, (%esi)
        # Emitting r2 = (((((((i0 + i1) + i2) + i3) + i4) + i5) + i6) + i7)
          # Emitting r2
          leal -8(%ebp), %edx
          # Emitting (((((((i0 + i1) + i2) + i3) + i4) + i5) + i6) + i7)
            # Emitting ((((((i0 + i1) + i2) + i3) + i4) + i5) + i6)
              # Emitting (((((i0 + i1) + i2) + i3) + i4) + i5)
                # Emitting ((((i0 + i1) + i2) + i3) + i4)
                  # Emitting (((i0 + i1) + i2) + i3)
                    # Emitting ((i0 + i1) + i2)
                      # Emitting (i0 + i1)
                        # Emitting i1
                        movl -20(%ebp), %esi
                        # Emitting i0
                        movl -16(%ebp), %edi
                      add %esi, %edi
                      # Emitting i2
                      movl -24(%ebp), %esi
                    add %esi, %edi
                    # Emitting i3
                    movl -28(%ebp), %esi
                  add %esi, %edi
                  # Emitting i4
                  movl -32(%ebp), %esi
                add %esi, %edi
                # Emitting i5
                movl -36(%ebp), %esi
              add %esi, %edi
              # Emitting i6
              movl -40(%ebp), %esi
            add %esi, %edi
            # Emitting i7
            movl -44(%ebp), %esi
          add %esi, %edi
        movl %edi, (%edx)
        # Emitting r1 = ((((((2 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7887))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))) + ((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7))))) + (((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))) + ((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7))))))
          # Emitting r1
          leal -4(%ebp), %edi
          # Emitting ((((((2 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7887))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))) + ((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7))))) + (((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))) + ((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7))))))
            # Emitting (((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))) + ((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))))
              # Emitting ((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7))))
                # Emitting (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %edx
                      # Emitting 6
                      movl $6, %esi
                    add %edx, %esi
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %edx
                      # Emitting 4
                      movl $4, %ecx
                    add %edx, %ecx
                  add %esi, %ecx
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %esi
                      # Emitting 6
                      movl $6, %edx
                    add %esi, %edx
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %esi
                      # Emitting 4
                      movl $4, %ebx
                    add %esi, %ebx
                  add %edx, %ebx
                add %ecx, %ebx
                # Emitting (((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7)))
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %ecx
                      # Emitting 6
                      movl $6, %edx
                    add %ecx, %edx
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %ecx
                      # Emitting 4
                      movl $4, %esi
                    add %ecx, %esi
                  add %edx, %esi
                  # Emitting ((0 + 1) + (2 + 3))
                    # Emitting (2 + 3)
                      # Emitting 3
                      movl $3, %edx
                      # Emitting 2
                      movl $2, %ecx
                    add %edx, %ecx
                    # Emitting (0 + 1)
                      # Emitting 1
                      movl $1, %edx
                      # Emitting 0
                      movl $0, %eax
                    add %edx, %eax
                  add %ecx, %eax
                add %esi, %eax
              add %ebx, %eax
              # Emitting ((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7))))
                # Emitting (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %ebx
                      # Emitting 6
                      movl $6, %esi
                    add %ebx, %esi
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %ebx
                      # Emitting 4
                      movl $4, %ecx
                    add %ebx, %ecx
                  add %esi, %ecx
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %esi
                      # Emitting 6
                      movl $6, %ebx
                    add %esi, %ebx
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %esi
                      # Emitting 4
                      movl $4, %edx
                    add %esi, %edx
                  add %ebx, %edx
                add %ecx, %edx
                # Emitting (((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7)))
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %ecx
                      # Emitting 6
                      movl $6, %ebx
                    add %ecx, %ebx
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %ecx
                      # Emitting 4
                      movl $4, %esi
                    add %ecx, %esi
                  add %ebx, %esi
                  # Emitting ((0 + 1) + (2 + 3))
                    # Emitting (2 + 3)
                      # Emitting 3
                      movl $3, %ebx
                      # Emitting 2
                      movl $2, %ecx
                    add %ebx, %ecx
                    # Emitting (0 + 1)
                      # Emitting 1
                      movl $1, %ebx
                      # Emitting 0
                      pushl %edi
                      movl $0, %edi
                    add %ebx, %edi
                  add %ecx, %edi
                add %esi, %edi
              add %edx, %edi
            add %eax, %edi
            # Emitting (((((2 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7887))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))) + ((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))))
              # Emitting ((((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7))))
                # Emitting (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %eax
                      # Emitting 6
                      movl $6, %edx
                    add %eax, %edx
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %eax
                      # Emitting 4
                      movl $4, %esi
                    add %eax, %esi
                  add %edx, %esi
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %edx
                      # Emitting 6
                      movl $6, %eax
                    add %edx, %eax
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %edx
                      # Emitting 4
                      movl $4, %ecx
                    add %edx, %ecx
                  add %eax, %ecx
                add %esi, %ecx
                # Emitting (((0 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7)))
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %esi
                      # Emitting 6
                      movl $6, %eax
                    add %esi, %eax
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %esi
                      # Emitting 4
                      movl $4, %edx
                    add %esi, %edx
                  add %eax, %edx
                  # Emitting ((0 + 1) + (2 + 3))
                    # Emitting (2 + 3)
                      # Emitting 3
                      movl $3, %eax
                      # Emitting 2
                      movl $2, %esi
                    add %eax, %esi
                    # Emitting (0 + 1)
                      # Emitting 1
                      movl $1, %eax
                      # Emitting 0
                      movl $0, %ebx
                    add %eax, %ebx
                  add %esi, %ebx
                add %edx, %ebx
              add %ecx, %ebx
              # Emitting ((((2 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7887))) + (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7))))
                # Emitting (((4 + 5) + (6 + 7)) + ((4 + 5) + (6 + 7)))
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %ecx
                      # Emitting 6
                      movl $6, %edx
                    add %ecx, %edx
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %ecx
                      # Emitting 4
                      movl $4, %esi
                    add %ecx, %esi
                  add %edx, %esi
                  # Emitting ((4 + 5) + (6 + 7))
                    # Emitting (6 + 7)
                      # Emitting 7
                      movl $7, %edx
                      # Emitting 6
                      movl $6, %ecx
                    add %edx, %ecx
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %edx
                      # Emitting 4
                      movl $4, %eax
                    add %edx, %eax
                  add %ecx, %eax
                add %esi, %eax
                # Emitting (((2 + 1) + (2 + 3)) + ((4 + 5) + (6 + 7887)))
                  # Emitting ((4 + 5) + (6 + 7887))
                    # Emitting (6 + 7887)
                      # Emitting 7887
                      movl $7887, %esi
                      # Emitting 6
                      movl $6, %ecx
                    add %esi, %ecx
                    # Emitting (4 + 5)
                      # Emitting 5
                      movl $5, %esi
                      # Emitting 4
                      movl $4, %edx
                    add %esi, %edx
                  add %ecx, %edx
                  # Emitting ((2 + 1) + (2 + 3))
                    # Emitting (2 + 3)
                      # Emitting 3
                      movl $3, %ecx
                      # Emitting 2
                      movl $2, %esi
                    add %ecx, %esi
                    # Emitting (2 + 1)
                      # Emitting 1
                      movl $1, %ecx
                      # Emitting 2
                      pushl %esi
                      movl $2, %esi
                    add %ecx, %esi
                  movl -68(%ebp), %ecx
                  add %ecx, %esi
                add %edx, %esi
              add %eax, %esi
            add %ebx, %esi
          add %edi, %esi
        movl %esi, (%edi)
        # Emitting write(r1)
          # Emitting r1
          movl -4(%ebp), %esi
        sub $16, %esp
        movl %esi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting write(r2)
          # Emitting r2
          movl -8(%ebp), %esi
        sub $16, %esp
        movl %esi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting write(r3)
          # Emitting r3
          movl -12(%ebp), %esi
        sub $16, %esp
        movl %esi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting writeln()
        sub $16, %esp
        movl $STR_NL, 0(%esp)
        call printf
        add $16, %esp
        # Emitting return
        movl $0, %eax
        leal 4(%ebp), %esp
        movl (%ebp), %ebp
        ret
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
