.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.int Main_a
.section .text
.globl main
main:
sub $16, %esp
movl $8, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting int a
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting b = 6
          # Emitting b
          leal -4(%ebp), %edi
          # Emitting 6
          movl $6, %esi
        movl %esi, (%edi)
        # Emitting this.a = 5
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %esi
          leal 4(%esi), %esi
          # Emitting 5
          movl $5, %edi
        movl %edi, (%esi)
        # Emitting write(this.a)
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %edi
          leal 4(%edi), %edi
          movl (%edi), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting write(b)
          # Emitting b
          movl -4(%ebp), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
        # Emitting this.a(...)
          # Emitting this
          movl 8(%ebp), %edi
        pushl %edi
        movl (%edi), %edi
        movl 8(%edi), %edi
        call *%edi
        # Emitting this.a = b
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %esi
          leal 4(%esi), %esi
          # Emitting b
          movl -4(%ebp), %edx
        movl %edx, (%esi)
        # Emitting write((this.a + b))
          # Emitting (this.a + b)
            # Emitting b
            movl -4(%ebp), %edx
            # Emitting this.a
              # Emitting this
              movl 8(%ebp), %esi
            leal 4(%esi), %esi
            movl (%esi), %esi
          add %edx, %esi
        sub $16, %esp
        movl %esi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
    pushl %edi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
    # Emitting void a(...) {...}
    Main_a:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting this.a = 8
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %edi
          leal 4(%edi), %edi
          # Emitting 8
          movl $8, %esi
        movl %esi, (%edi)
        # Emitting write(this.a)
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %esi
          leal 4(%esi), %esi
          movl (%esi), %esi
        sub $16, %esp
        movl %esi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
