.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
CLASS_A:
.int 0
.int A_b
.section .text
.globl main
main:
sub $16, %esp
movl $8, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting A a
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting this.a = new A()
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %edi
          leal 4(%edi), %edi
          # Emitting new A()
          sub $16, %esp
          movl $8, 0(%esp)
          call calloc
          add $16, %esp
          leal (CLASS_A), %ebx
          movl %ebx, (%eax)
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting this.a.b(...)
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %esi
          leal 4(%esi), %esi
          movl (%esi), %esi
        pushl %esi
        movl (%esi), %esi
        movl 4(%esi), %esi
        call *%esi
        # Emitting write(this.a.a)
          # Emitting this.a.a
            # Emitting this.a
              # Emitting this
              movl 8(%ebp), %edi
            leal 4(%edi), %edi
            movl (%edi), %edi
          leal 4(%edi), %edi
          movl (%edi), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
    pushl %esi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
  # Emitting class A {...}
    # Emitting int a
    # Emitting void b(...) {...}
    A_b:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting this.a = 4
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %esi
          leal 4(%esi), %esi
          # Emitting 4
          movl $4, %edi
        movl %edi, (%esi)
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
