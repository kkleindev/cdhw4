.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $4, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
    pushl $0
      # Emitting (...)
        # Emitting i = 1
          # Emitting i
          leal -4(%ebp), %edi
          # Emitting 1
          movl $1, %esi
        movl %esi, (%edi)
        # Emitting while ((i < 10)) {...}
          # Emitting (i < 10)
            # Emitting 10
            movl $10, %esi
            # Emitting i
            movl -4(%ebp), %edi
          cmp %esi, %edi
          jl label3
          movl $0, %edi
          jmp label2
          label3:
          movl $1, %edi
          label2:
        movl $1, %esi
        cmpl %edi, %esi
        jg label1
        label0:
          # Emitting (...)
            # Emitting write(i)
              # Emitting i
              movl -4(%ebp), %edi
            sub $16, %esp
            movl %edi, 4(%esp)
            movl $STR_D, 0(%esp)
            call printf
            add $16, %esp
            # Emitting i = (i + 1)
              # Emitting i
              leal -4(%ebp), %edi
              # Emitting (i + 1)
                # Emitting 1
                movl $1, %edx
                # Emitting i
                movl -4(%ebp), %ecx
              add %edx, %ecx
            movl %ecx, (%edi)
          # Emitting (i < 10)
            # Emitting 10
            movl $10, %ecx
            # Emitting i
            movl -4(%ebp), %edi
          cmp %ecx, %edi
          jl label7
          movl $0, %edi
          jmp label6
          label7:
          movl $1, %edi
          label6:
        cmp %edi, %esi
        jg label1
        jmp label0
        label1:
    pushl %edi
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
