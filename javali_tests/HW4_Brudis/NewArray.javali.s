.section .data
STR_NL:
.string "\n"
STR_D:
.string "%d"
.section .data
CLASS_Main:
.int 0
.int Main_main
.section .text
.globl main
main:
sub $16, %esp
movl $8, 0(%esp)
call calloc
add $16, %esp
leal (CLASS_Main), %ebx
movl %ebx, (%eax)
pushl %eax
call Main_main
pushl $0
call exit
  # Emitting class Main {...}
    # Emitting int[] a
    # Emitting void main(...) {...}
    Main_main:
    pushl %ebp
    movl %esp, %ebp
      # Emitting (...)
        # Emitting this.a = new int[][5]
          # Emitting this.a
            # Emitting this
            movl 8(%ebp), %edi
          leal 4(%edi), %edi
          # Emitting new int[][5]
            # Emitting 5
            movl $5, %esi
          sub $16, %esp
          imul $4, %esi
          movl %esi, (%esp)
          call calloc
          add $16, %esp
          movl %eax, %esi
        movl %esi, (%edi)
        # Emitting this.a[4] = 3
          # Emitting this.a[4]
            # Emitting 4
            movl $4, %esi
            # Emitting this.a
              # Emitting this
              movl 8(%ebp), %edi
            leal 4(%edi), %edi
            movl (%edi), %edi
          leal (%edi, %esi, 4), %edi
          # Emitting 3
          movl $3, %esi
        movl %esi, (%edi)
        # Emitting write(this.a[4])
          # Emitting this.a[4]
            # Emitting 4
            movl $4, %esi
            # Emitting this.a
              # Emitting this
              movl 8(%ebp), %edi
            leal 4(%edi), %edi
            movl (%edi), %edi
          movl (%edi, %esi, 4), %edi
        sub $16, %esp
        movl %edi, 4(%esp)
        movl $STR_D, 0(%esp)
        call printf
        add $16, %esp
    movl $0, %eax
    leal 4(%ebp), %esp
    movl (%ebp), %ebp
    ret
